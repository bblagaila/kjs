using System;

using kjs.parser;
using kjs.parser.Value;
using kjs.parser.Compiler;
using System.Diagnostics;

namespace kjs {

class MainClass {
    public static JSValue consoleLog(JSValue _this, JSValue[] arguments) {
        JSValue ret = JSValue.Undefined();

        string msg = arguments[0].toString();

        Console.WriteLine(msg);

        return ret;
    }

    public static void addNativeApi(JSVM vm) {
        JSValueFunction console_log_func = JSValueFunction.createNative("log", consoleLog);
        JSValueObject console = new JSValueObject();

        console.set("log", console_log_func);

        vm.global_scope.set("console", console);
    }

    public static int loadFile(JSVM vm, string filename) {
        int ret = -1;
        System.IO.Stream file = null;
        JSCode code = null;
        JSCompiler compiler = null;

        Console.WriteLine("Loading " + filename + "...");

        file = System.IO.File.OpenRead(filename);

        Console.WriteLine("Compiling...");

        compiler = new JSCompiler();
        code = compiler.compile(file);

        foreach (var message in compiler.messages) {
            string line = "";

            switch (message.level) {
                case JSCompiler.CompilerMessage.Level.ERROR:
                    line += "[ERROR]";
                    break;

                case JSCompiler.CompilerMessage.Level.WARNING:
                    line += "[WARNING]";
                    break;

                case JSCompiler.CompilerMessage.Level.FATAL:
                    line += "[FATAL]";
                    break;

                default:
                    line += "[INFO]";
                    break;
            }

            line += "[" + message.line_number + ":" + message.line_offset + "]";

            line += ": " + message.message;

            if (message.offending_context.Length > 0) {
                line += "\n";
                line += message.offending_context + "\n";

                for (int i = 0; i < message.line_offset; i++) {
                    line += " ";
                }
                line += "^";
            }

            Console.WriteLine(line);
        }

        if (code != null) {
            OpCodes op = new OpCodes();
            Console.WriteLine(OpCodes.dumpCode(code.code));

            Console.WriteLine("Evaluating...");

            Stopwatch sw = new Stopwatch();
            sw.Start();

            vm.eval(code);
            vm.executeAll();

            sw.Stop();
            Console.WriteLine("Time: " + sw.ElapsedMilliseconds + " ms.");

            ret = 0;
        }
        else {
            ret = -1;
        }

        Console.WriteLine("Done.");

        return ret;
    }

	public static void Main (string[] args) {
        JSVM vm = new JSVM();
        addNativeApi(vm);

		string filename = "../../tests/test7.js";

        loadFile(vm, filename);
	}
}

}
