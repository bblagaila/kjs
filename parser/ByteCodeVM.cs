﻿//
//  ByteCodeVM.cs
//
//  Author:
//       Bogdan <${AuthorEmail}>
//
//  Copyright (c) 2015 Bogdan
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  aint with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//

using System;
using System.Collections.Generic;

using kjs.parser.Value;

namespace kjs.parser {

public class ByteCodeVM {
	public class CallFrame {
		public byte[] code;
		public int ip;
		public int last_ip = 0;

		public JSValueObject scope;
		public JSValueObject this_object;
		public JSValueFunction current_function;
		public bool is_constructor = false;

		public int previous_stack_size = 0;

		public OpCodes.OpCode getOpCode() {
			this.last_ip = this.ip;
			return (OpCodes.OpCode)this.code[this.ip++];
		}

		public int unpackNumber() {
			int number = BitConverter.ToInt32(this.code, this.ip);
			this.ip += sizeof(int);
			return number;
		}

		public float unpackFloatNumber() {
			float number = BitConverter.ToSingle(this.code, this.ip);
			this.ip += sizeof(float);
			return number;
		}

		public string unpackString() {
			int start_position = this.ip;
			for (; this.ip < this.code.Length; this.ip++) {
				if (this.code[this.ip] == 0) {
					break;
				}
			}
			string str = new string(System.Text.Encoding.UTF8.GetChars(this.code, (int)start_position, (int)(this.ip - start_position)));
			this.ip += 1;
			return str;
		}
	};

	public class JSEvalStack {
		public Stack<JSValue> eval_stack = new Stack<JSValue>();
		public JSEvalStack() {
		}
		public JSValue Pop() {
			JSValue ret = this.eval_stack.Pop();
			return ret;
		}
		public JSValue Peek() {
			return this.eval_stack.Peek();
		}
		public void Push(JSValue v1) {
			this.eval_stack.Push(v1);
		}

		public int Count() {
			return this.eval_stack.Count;
		}
	};

	public JSValueObject global_scope = new JSValueObject();
	public Stack<CallFrame> call_stack = new Stack<CallFrame>();
	public JSEvalStack eval_stack = new JSEvalStack();

	public ByteCodeVM() {
	}

	public void evalProgram(Program program) {
		CallFrame global_frame = new CallFrame();

		global_frame.code = program.code;
		global_frame.ip = 0;
		global_frame.scope = this.global_scope;
		global_frame.this_object = this.global_scope;
		global_frame.current_function = null;

		call_stack.Push(global_frame);
	}

	public void execute() {
		CallFrame current_frame = this.call_stack.Peek();

		while (current_frame.ip < current_frame.code.Length) {
			this.executeOpCode();
			current_frame = this.call_stack.Peek();
		}
	}

	public void executeOpCode() {
		CallFrame current_frame = this.call_stack.Peek();
		OpCodes.OpCode op_code = current_frame.getOpCode();

		switch (op_code) {
			case OpCodes.OpCode.PUSH_NULL:
				eval_stack.Push(JSValue.Null());
				break;

			case OpCodes.OpCode.PUSH_UNDEFINED:
				eval_stack.Push(JSValue.Undefined());
				break;

			case OpCodes.OpCode.PUSH_BOOLEAN_TRUE:
				eval_stack.Push(new JSValueBoolean(true));
				break;

			case OpCodes.OpCode.PUSH_BOOLEAN_FALSE:
				eval_stack.Push(new JSValueBoolean(false));
				break;

			case OpCodes.OpCode.PUSH_NUMBER:
				eval_stack.Push(new JSValueNumber(current_frame.unpackFloatNumber()));
				break;
			
			case OpCodes.OpCode.PUSH_STRING:
				eval_stack.Push(new JSValueString(current_frame.unpackString()));
				break;

			case OpCodes.OpCode.PUSH_REGEX:
				// TODO: Later ... this object is complicated...
				break;

			case OpCodes.OpCode.PUSH_ARRAY:
				// TODO: Later ... this object is complicated...
				break;

			case OpCodes.OpCode.PUSH_OBJECT:
				{
					JSValueObject obj = new JSValueObject();
					int number_of_items = current_frame.unpackNumber();
					for (int i = 0; i < number_of_items; i++) {
						JSValue value = this.referenceGet(this.eval_stack.Pop());
						string property_name = this.eval_stack.Pop().toString();
						obj.set(property_name, value);
					}
					eval_stack.Push(obj);
				}
				break;

			case OpCodes.OpCode.PUSH_FUNCTION:
				{
					int function_size = current_frame.unpackNumber();
					int function_end_index = current_frame.ip + function_size;

					string function_name = current_frame.unpackString();

					JSValueFunction function = new JSValueFunction();
					function.name = function_name;

					int nr_parameters = current_frame.unpackNumber();
					function.formal_parameters = new string[nr_parameters];
					for (int i = 0; i < nr_parameters; i++) {
						function.formal_parameters[i] = current_frame.unpackString();
					}

					int nr_local_variables = current_frame.unpackNumber();
					function.local_variables = new string[nr_local_variables];
					for (int i = 0; i < nr_local_variables; i++) {
						function.local_variables[i] = current_frame.unpackString();
					}

					function.code_ref = current_frame.code;
					function.code_start_offset = current_frame.ip;

					current_frame.ip = function_end_index;

					eval_stack.Push(function);

					if (function.name.Length > 0) {
						JSValueReference reference = new JSValueReference(null, function.name);
						this.referenceSet(reference, function);
					}

					/* Create the closure */
					int nr_closures = 0;

					if (current_frame.current_function != null) {
						nr_closures = current_frame.current_function.closure.Length;
					}
					else {
						nr_closures = 1;
					}

					JSValueObject[] closures = new JSValueObject[nr_closures];
					for (int i = 0; i < nr_closures - 1; i++) {
						closures[0] = current_frame.current_function.closure[i];
					}
					closures[nr_closures - 1] = current_frame.scope;

					function.closure = closures;
				}
				break;
			
			case OpCodes.OpCode.PUSH_IDENTIFIER:
				eval_stack.Push(new JSValueReference(null, current_frame.unpackString()));
				break;

			case OpCodes.OpCode.PUSH_THIS:
				eval_stack.Push(current_frame.this_object);
				break;

			case OpCodes.OpCode.POST_INC:
			case OpCodes.OpCode.POST_DEC:
				/*
				 * (ref) 				; (expr ref)
				 * (ref) (ref) 			; DUP
				 * (ref) (ref) (ref)	; DUP
				 * (ref) (ref) (value)  ; DEREF
				 * (ref) (ref) (value+1); INC
				 * (ref) (value + 1)	; ASSIGN
				 * (ref)				; POP
				 * (value + 1)			; DEREF -- shit ...
				 */
				{
					JSValue value = this.eval_stack.Pop();
					JSValue old_value = this.referenceGet(value);

					if (value.type != JSValue.Type.REFERENCE) {
						emitRuntimeError("Reference Error: Invalid left-hand side expression: " + value.toString());
						break;
					}

					eval_stack.Push(old_value);

					JSValueNumber new_value = new JSValueNumber(old_value);
					if (op_code == OpCodes.OpCode.POST_INC) {
						new_value.numberValue++;
					}
					else {
						new_value.numberValue--;
					}
					this.referenceSet(value, new_value);
				}
				break;

			case OpCodes.OpCode.CALL:
			case OpCodes.OpCode.NEW:
				{
					int num_parameters = current_frame.unpackNumber();
					JSValue[] parameters = new JSValue[num_parameters];
					for (int i = 0; i < num_parameters; i++) {
						parameters[i] = this.referenceGet(this.eval_stack.Pop());
					}

					bool is_constructor = false;
					JSValue fnRef = this.eval_stack.Pop();
					JSValue candidate = this.referenceGet(fnRef);
					JSValueFunction function = null;

					if (candidate.type != JSValue.Type.FUNCTION) {
						emitRuntimeError("TypeError: " + candidate.getTypeString() + " is not a function");
						break;
					}

					function = (JSValueFunction)candidate;

					JSValueObject _this = this.global_scope;

					if (op_code == OpCodes.OpCode.NEW) {
						is_constructor = true;
						_this = new JSValueObject();

						if (function.name.Length > 0) {
							_this.name = function.name;
						}
						else if (fnRef.type == JSValue.Type.REFERENCE) {
							_this.name = ((JSValueReference)fnRef).refProperty;
						}

						JSValue __proto = function.get("prototype");
						if (__proto != null && __proto.type == JSValue.Type.OBJECT) {
							JSValueObject prototype = (JSValueObject)__proto;

							foreach (KeyValuePair<string, JSValue> pair in prototype.objectValue) {
								_this.set(pair.Key, pair.Value);
							}
						}
					}
					else if (fnRef.type == JSValue.Type.REFERENCE) {
						JSValueReference r = (JSValueReference)fnRef;
						if (r.refObject != null) {
							_this = (JSValueObject)r.refObject;
						}
					}

					this.callFunction(function, _this, parameters, is_constructor);
				}
				break;

			case OpCodes.OpCode.MEMBER_ID:
				{
					JSValueReference property_name = (JSValueReference)this.eval_stack.Pop();
					JSValue parent = this.referenceGet(this.eval_stack.Pop());

					eval_stack.Push(new JSValueReference(parent, property_name.refProperty));
				}
				break;

			case OpCodes.OpCode.MEMBER:
				{
					JSValue property_name = this.eval_stack.Pop();
					JSValue parent = this.referenceGet(this.eval_stack.Pop());

					eval_stack.Push(new JSValueReference(parent, property_name.toString()));
				}
				break;

			case OpCodes.OpCode.INC:
			case OpCodes.OpCode.DEC:
				{
					JSValue value = this.eval_stack.Pop();

					if (value.type != JSValue.Type.REFERENCE) {
						emitRuntimeError("Reference Error: Invalid left-hand side expression: " + value.toString());
						break;
					}

					JSValueNumber new_value = new JSValueNumber(this.referenceGet(value));
					if (op_code == OpCodes.OpCode.INC) {
						new_value.numberValue++;
					}
					else {
						new_value.numberValue--;
					}
					this.referenceSet(value, new_value);
					eval_stack.Push(new_value);
				}
				break;

			case OpCodes.OpCode.MINUS:
				{
					JSValueNumber value = new JSValueNumber(this.referenceGet(this.eval_stack.Pop()));
					value.numberValue = -value.numberValue;
					eval_stack.Push(value);
				}
				break;

			case OpCodes.OpCode.PLUS:
				/* Kinda does nothing */
				break;

			case OpCodes.OpCode.B_NOT:
				{
					JSValueNumber value = new JSValueNumber(this.referenceGet(this.eval_stack.Pop()));
					value.numberValue = (float)(~((int)value.numberValue));
					eval_stack.Push(value);
				}
				break;

			case OpCodes.OpCode.L_NOT:
				{
					JSValueBoolean value = new JSValueBoolean(this.referenceGet(this.eval_stack.Pop()));
					eval_stack.Push(new JSValueBoolean(!value.booleanValue));
				}
				break;

			case OpCodes.OpCode.DELETE:
				{
					JSValue val = this.eval_stack.Pop();
					if (val.type == JSValue.Type.REFERENCE) {
						this.referenceDelete((JSValueReference)val);
					}
					this.eval_stack.Push(JSValueBoolean.True());
				}
				break;

			case OpCodes.OpCode.TYPEOF:
				eval_stack.Push(new JSValueString(this.referenceGet(this.eval_stack.Pop()).getTypeString()));
				break;

			case OpCodes.OpCode.MULT:
				{
					JSValue v2 = this.referenceGet(this.eval_stack.Pop());
					JSValue v1 = this.referenceGet(this.eval_stack.Pop());
					eval_stack.Push(JSValueOperations.valuesMult(v1, v2));
				}
				break;

			case OpCodes.OpCode.DIV:
				{
					JSValue v2 = this.referenceGet(this.eval_stack.Pop());
					JSValue v1 = this.referenceGet(this.eval_stack.Pop());
					eval_stack.Push(JSValueOperations.valuesDiv(v1, v2));
				}
				break;

			case OpCodes.OpCode.REM:
				{
					JSValue v2 = this.referenceGet(this.eval_stack.Pop());
					JSValue v1 = this.referenceGet(this.eval_stack.Pop());
					eval_stack.Push(JSValueOperations.valuesRem(v1, v2));
				}
				break;

			case OpCodes.OpCode.ADD:
				{
					JSValue v2 = this.referenceGet(this.eval_stack.Pop());
					JSValue v1 = this.referenceGet(this.eval_stack.Pop());
					eval_stack.Push(JSValueOperations.valuesAdd(v1, v2));
				}
				break;

			case OpCodes.OpCode.SUB:
				{
					JSValue v2 = this.referenceGet(this.eval_stack.Pop());
					JSValue v1 = this.referenceGet(this.eval_stack.Pop());
					eval_stack.Push(JSValueOperations.valuesSub(v1, v2));
				}
				break;

			case OpCodes.OpCode.LSHIFT:
				{
					JSValue amount = this.referenceGet(this.eval_stack.Pop());
					JSValue value = this.referenceGet(this.eval_stack.Pop());
					eval_stack.Push(JSValueOperations.valueLShift(value, amount));
				}
				break;

			case OpCodes.OpCode.RSHIFT:
				{
					JSValue amount = this.referenceGet(this.eval_stack.Pop());
					JSValue value = this.referenceGet(this.eval_stack.Pop());
					eval_stack.Push(JSValueOperations.valueRShift(value, amount));
				}
				break;

			case OpCodes.OpCode.ZFRSHIFT:
				{
					JSValue amount = this.referenceGet(this.eval_stack.Pop());
					JSValue value = this.referenceGet(this.eval_stack.Pop());
					eval_stack.Push(JSValueOperations.valueZFRShift(value, amount));
				}
				break;

			case OpCodes.OpCode.IS_LT:
				{
					JSValue v2 = this.referenceGet(this.eval_stack.Pop());
					JSValue v1 = this.referenceGet(this.eval_stack.Pop());
					bool res = JSValueOperations.valuesCompare(v1, v2) < 0.0f;
					eval_stack.Push(new JSValueBoolean(res));
				}
				break;
			
			case OpCodes.OpCode.IS_GT:
				{
					JSValue v2 = this.referenceGet(this.eval_stack.Pop());
					JSValue v1 = this.referenceGet(this.eval_stack.Pop());
					bool res = JSValueOperations.valuesCompare(v1, v2) > 0.0f;
					eval_stack.Push(new JSValueBoolean(res));
				}
				break;

			case OpCodes.OpCode.IS_LTE:
				{
					JSValue v2 = this.referenceGet(this.eval_stack.Pop());
					JSValue v1 = this.referenceGet(this.eval_stack.Pop());
					bool res = JSValueOperations.valuesCompare(v1, v2) <= 0.0f;
					eval_stack.Push(new JSValueBoolean(res));
				}
				break;

			case OpCodes.OpCode.IS_GTE:
				{
					JSValue v2 = this.referenceGet(this.eval_stack.Pop());
					JSValue v1 = this.referenceGet(this.eval_stack.Pop());
					bool res = JSValueOperations.valuesCompare(v1, v2) >= 0.0f;
					eval_stack.Push(new JSValueBoolean(res));
				}
				break;

			case OpCodes.OpCode.INSTANCEOF:
				{
					JSValue v2 = this.referenceGet(this.eval_stack.Pop());
					JSValue v1 = this.referenceGet(this.eval_stack.Pop());
					// TODO: implement this later, once we have the prototype
				}
				break;

			case OpCodes.OpCode.IN:
				{
					JSValue v2 = this.referenceGet(this.eval_stack.Pop());
					JSValue v1 = this.referenceGet(this.eval_stack.Pop());
					// TODO: implement this later, once we have array objects...
				}
				break;

			case OpCodes.OpCode.IS_EQ:
				{
					JSValue v2 = this.referenceGet(this.eval_stack.Pop());
					JSValue v1 = this.referenceGet(this.eval_stack.Pop());
					bool res = JSValueOperations.valuesCompare(v1, v2).Equals(0.0f);
					eval_stack.Push(new JSValueBoolean(res));
				}
				break;

			case OpCodes.OpCode.IS_NEQ:
				{
					JSValue v2 = this.referenceGet(this.eval_stack.Pop());
					JSValue v1 = this.referenceGet(this.eval_stack.Pop());
					bool res = !JSValueOperations.valuesCompare(v1, v2).Equals(0.0f);
					eval_stack.Push(new JSValueBoolean(res));
				}
				break;

			case OpCodes.OpCode.IS_SEQ:
				{
					JSValue v2 = this.referenceGet(this.eval_stack.Pop());
					JSValue v1 = this.referenceGet(this.eval_stack.Pop());
					bool res = JSValueOperations.valuesStrictCompare(v1, v2).Equals(0.0f);
					eval_stack.Push(new JSValueBoolean(res));
				}
				break;

			case OpCodes.OpCode.IS_NSEQ:
				{
					JSValue v2 = this.referenceGet(this.eval_stack.Pop());
					JSValue v1 = this.referenceGet(this.eval_stack.Pop());
					bool res = !JSValueOperations.valuesStrictCompare(v1, v2).Equals(0.0f);
					eval_stack.Push(new JSValueBoolean(res));
				}
				break;

			case OpCodes.OpCode.B_AND:
				{
					JSValue v2 = this.referenceGet(this.eval_stack.Pop());
					JSValue v1 = this.referenceGet(this.eval_stack.Pop());
					eval_stack.Push(JSValueOperations.valuesBitwiseAnd(v1, v2));
				}
				break;

			case OpCodes.OpCode.B_OR:
				{
					JSValue v2 = this.referenceGet(this.eval_stack.Pop());
					JSValue v1 = this.referenceGet(this.eval_stack.Pop());
					eval_stack.Push(JSValueOperations.valuesBitwiseOr(v1, v2));
				}
				break;

			case OpCodes.OpCode.B_XOR:
				{
					JSValue v2 = this.referenceGet(this.eval_stack.Pop());
					JSValue v1 = this.referenceGet(this.eval_stack.Pop());
					eval_stack.Push(JSValueOperations.valuesBitwiseXor(v1, v2));
				}
				break;

			case OpCodes.OpCode.L_AND:
				{
					JSValue v2 = this.referenceGet(this.eval_stack.Pop());
					JSValue v1 = this.referenceGet(this.eval_stack.Pop());
					eval_stack.Push(JSValueOperations.valuesLogicalAnd(v1, v2));
				}
				break;

			case OpCodes.OpCode.L_OR:
				{
					JSValue v2 = this.referenceGet(this.eval_stack.Pop());
					JSValue v1 = this.referenceGet(this.eval_stack.Pop());
					eval_stack.Push(JSValueOperations.valuesLogicalOr(v1, v2));
				}
				break;

			case OpCodes.OpCode.ASSIGN:
				{
					JSValue value = this.referenceGet(this.eval_stack.Pop());
					JSValue target = this.eval_stack.Pop();

					if (target.type != JSValue.Type.REFERENCE) {
						emitRuntimeError("Reference Error: Invalid left-hand side in assignment: " + target.toString());
						break;
					}
					this.referenceSet(target, value);
					eval_stack.Push(value);
				}
				break;

			case OpCodes.OpCode.RET:
				{
					JSValue return_value = this.referenceGet(this.eval_stack.Pop());
					this.returnFromFunction(return_value);
				}
				break;

			case OpCodes.OpCode.JMP:
				{
					int relative_position = (int)current_frame.unpackNumber();
					current_frame.ip += relative_position;
				}
				break;

			case OpCodes.OpCode.JF:
				{
					JSValueBoolean cond = new JSValueBoolean(this.referenceGet(this.eval_stack.Pop()));
					int relative_position = (int)current_frame.unpackNumber();
					if (!cond.getValue()) {
						current_frame.ip += relative_position;
					}
				}
				break;
		}
	}

	public JSValue getReferenceParent(JSValueReference reference) {
		if (reference.refObject != null) {
			return reference.refObject;
		}

		CallFrame current_frame = this.call_stack.Peek();
		JSValue value = null;

		if (current_frame != null) {
			/* 1. First try current stack scope */
			value = current_frame.scope.get(reference.refProperty);
			if (value != null) {
				return current_frame.scope;
			}

			/* 2. Try the closures */
			if (current_frame.current_function != null) {
				JSValueObject[] closures = current_frame.current_function.closure;

				for (int i = closures.Length - 1; i >= 0; i--) {
					value = closures[i].get(reference.refProperty);
					if (value != null) {
						return closures[i];
					}
				}
			}
		}

		/* 3. Try the global scope */
		return this.global_scope;
	}

	public JSValue referenceGet(JSValue reference) {
		JSValue parent = null;
		JSValue value = null;

		if (reference.type != JSValue.Type.REFERENCE) {
			/* It's actually the value */
			return reference;
		}

		parent = this.getReferenceParent((JSValueReference)reference);

		if (parent.isObject()) {
			value = ((JSValueObject)parent).get(((JSValueReference)reference).refProperty);
		}
		else {
			// TODO: Some objects can be converted to objects ? eg.: Number() object ?
		}

		/*
		switch (parent.type) {
			case JSValue.Type.OBJECT:
				value = ((JSValueObject)parent).get(((JSValueReference)reference).refProperty);
				break;

			default:
				// TODO: Later we will support some other types, such as array, string and number.
				break;
		}
		*/

		if (value == null) {
			this.emitRuntimeError("ReferenceError: " + ((JSValueReference)reference).refProperty + " is not defined");
		}
		return value;
	}

	public void referenceSet(JSValue reference, JSValue value) {
		JSValue parent = null;

		if (reference.type != JSValue.Type.REFERENCE) {
			throw new Exception("internal error: cannot do referenceSet()");
		}

		parent = this.getReferenceParent((JSValueReference)reference);

		if (parent.isObject()) {
			((JSValueObject)parent).set(((JSValueReference)reference).refProperty, value);
		}

		/*
		switch (parent.type) {
			case JSValue.Type.OBJECT:
				((JSValueObject)parent).set(((JSValueReference)reference).refProperty, value);
				break;

			default:
				// TODO: Later we will support some other types, such as array, string and number.
				break;
		}
		*/
	}

	public void referenceDelete(JSValue reference) {
		JSValue parent = null;
		JSValueReference r = null;

		if (reference.type != JSValue.Type.REFERENCE) {
			/* Do nothing */
			return;
		}

		r = (JSValueReference)reference;
		parent = this.getReferenceParent(r);

		if (parent.isObject()) {
			((JSValueObject)parent).delete(r.refProperty);
		}

		/*
		switch (parent.type) {
			case JSValue.Type.OBJECT:
				((JSValueObject)parent).delete(r.refProperty);
				break;

			default:
				// TODO: Later when we will support some other types ...
				break;
		}
		*/
	}

	public void callFunction(JSValueFunction function, JSValueObject _this, JSValue[] parameters, bool is_constructor) {
		/* If the function is native, call it directly and blocking ... */
		if (function.isNative()) {
			JSValue return_value = function.native_function(_this, parameters);
			if (return_value == null) {
				this.eval_stack.Push(JSValue.Undefined());
			}
			else {
				this.eval_stack.Push(return_value);
			}
			return;
		}

		CallFrame frame = new CallFrame();

		frame.current_function = function;
		frame.is_constructor = is_constructor;
		frame.code = this.call_stack.Peek().code;
		frame.ip = function.code_start_offset;
		frame.scope = new JSValueObject();
		frame.this_object = _this;
		frame.previous_stack_size = this.eval_stack.Count();

		call_stack.Push(frame);

		/* Add local variables to local scope */
		for (int i = 0; i < function.local_variables.Length; i++) {
			frame.scope.set(function.local_variables[i], JSValue.Undefined());
		}

		/* Add parameters to local scope */
		for (int i = 0; i < function.formal_parameters.Length; i++) {
			if (i < parameters.Length) {
				frame.scope.set(function.formal_parameters[i], parameters[i]);
			}
			else {
				frame.scope.set(function.formal_parameters[i], JSValue.Undefined());
			}
		}
	}

	public void returnFromFunction(JSValue return_value) {
		int previous_stack_size = this.call_stack.Peek().previous_stack_size;
		while (this.eval_stack.Count() > previous_stack_size) {
			this.eval_stack.Pop();
		}

		if (this.call_stack.Peek().is_constructor && return_value.type != JSValue.Type.OBJECT) {
			this.eval_stack.Push(this.call_stack.Peek().this_object);
		}
		else {
			this.eval_stack.Push(return_value);
		}

		this.call_stack.Pop();
	}

	public void emitRuntimeError(string message) {
		// TODO: Support JS exceptions and throw those...
		Console.WriteLine("Unhandled Exception at " + this.call_stack.Peek().last_ip);
		Console.WriteLine("Exception: " + message);
	}

	public void dumpGlobalScope() {
		this.dumpObject(this.global_scope);
	}
	public void dumpObject(JSValue value, int indent = 0) {
		if (value.type != JSValue.Type.OBJECT) {
			Console.Write(value.toString());
			return;
		}

		Console.WriteLine("");
		foreach (KeyValuePair<string, JSValue> pair in ((JSValueObject)value).objectValue) {
			for (int i = 0; i < indent; i++) { Console.Write(" "); }

			Console.Write("" + pair.Key + " = ");
			dumpObject(pair.Value, indent + 4);
			Console.WriteLine("");
		}
	}
}

}
