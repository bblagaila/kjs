﻿/*
 *  JSCompiler.cs
 *
 *  Author:
 *       Bogdan Blagaila <blbogdan2000@gmail.com>
 *
 *  Copyright (c) 2014 Bogdan Blagaila
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

using System;
using System.Collections.Generic;
using Antlr4.Runtime;
using Antlr4.Runtime.Misc;

namespace kjs.parser.Compiler {

class JSCompiler {
	/** Struct for logging compiler messages */
	public struct CompilerMessage {
		public enum Level {
			NONE = 0,
			WARNING,
			ERROR,
			FATAL
		};

		public Level level;
		public string message;
		public int line_number;
		public int line_offset;
		public string offending_context;
	};

	/** Compiler messages */
	public List<CompilerMessage> messages = new List<CompilerMessage>();
    public int num_errors = 0;

    /**
     * Antlr4 Syntax error listener
     */
    public class AntlrSyntaxErrorLogger : Antlr4.Runtime.BaseErrorListener {
        JSCompiler compiler;

        public AntlrSyntaxErrorLogger(JSCompiler compiler) {
            this.compiler = compiler;
        }

        public override void SyntaxError(IRecognizer recognizer, IToken offendingSymbol, int line, int charPositionInLine, string msg, RecognitionException e) {
            string error_message = "Syntax error: " + msg + ": " + offendingSymbol.Text;
            this.compiler.logCompilationMessage(CompilerMessage.Level.ERROR, error_message, line, charPositionInLine, "");
        }
    }

    /**
     * Antlr4 error strategy for Automatic semi-colon insertion
     */
    public class AntlrAutomaticSemicolonInsertion : Antlr4.Runtime.DefaultErrorStrategy {
        public CommonTokenStream token_stream = null;

		public AntlrAutomaticSemicolonInsertion(CommonTokenStream token_stream) {
			this.token_stream = token_stream;
		}

		protected bool ShouldInsertAutoSemicolon(Parser recognizer, IToken offendingToken) {
			RuleContext context = recognizer.Context;
			while (context != null && context.RuleIndex != JSGrammarParser.RULE_statement) {
				context = context.Parent;
			}

			if (context != null && context.RuleIndex == JSGrammarParser.RULE_statement) {
				if (offendingToken.Type == JSGrammarLexer.CLOSE_CURLY_BRACKET) {
					return true;
				}
				else {
					IList<IToken> lt = this.token_stream.GetHiddenTokensToLeft(offendingToken.TokenIndex, 2);
					if (lt != null && lt.Count > 0) {
						return true;
					}
				}
			}

			return false;
		}

		protected IToken ConstructSemiColon(ITokenSource token_source, IToken current) {
			ITokenFactory token_factory = token_source.TokenFactory;
			IToken ret = token_factory.Create(JSGrammarLexer.SEMICOLON, "<missing ;>");
			return ret;
		}

		public override IToken RecoverInline(Parser recognizer) {
			if (this.ShouldInsertAutoSemicolon(recognizer, recognizer.CurrentToken)) {
				return ConstructSemiColon(recognizer.TokenStream.TokenSource, recognizer.CurrentToken);
			}
			return base.RecoverInline(recognizer);
		}
			
		protected override void ReportInputMismatch(Parser recognizer, InputMismatchException e) {
			if (this.ShouldInsertAutoSemicolon(recognizer, e.OffendingToken) == false) {
				base.ReportInputMismatch(recognizer, e);
			}
		}
			
		protected override void ReportUnwantedToken(Parser recognizer) {
			if (this.ShouldInsertAutoSemicolon(recognizer, recognizer.CurrentToken) == false) {
				base.ReportUnwantedToken(recognizer);
			}
		}
    }

    /**
     * Default constructor
     */
    public JSCompiler() {
    }

    /**
     * Compiles the given JavaScript code.
     */
    public JSCode compile(System.IO.Stream input) {
        JSCode ret = null;
        AntlrInputStream istream = null;
        JSGrammarLexer lexer = null;
        CommonTokenStream token_stream = null;
        JSGrammarParser parser = null;
        AntlrSyntaxErrorLogger syntax_error_logger = null;
        AntlrAutomaticSemicolonInsertion semicolon_insertion_error_strategy = null;
        JSGrammarParser.ProgramContext ast = null;
        JSCompilerASTVisitor ast_visitor = null;
        JSCompilerResult ast_result = null;

        /* Reset any previous errors */
        this.messages.Clear();
        this.num_errors = 0;

        /* 1. Lexer step */
        istream = new AntlrInputStream(input);
        lexer = new JSGrammarLexer(istream);
        token_stream = new CommonTokenStream(lexer);

        /* 2. Parser step */
        syntax_error_logger = new AntlrSyntaxErrorLogger(this);
        semicolon_insertion_error_strategy = new AntlrAutomaticSemicolonInsertion(token_stream);
        parser = new JSGrammarParser(token_stream);
        parser.AddErrorListener(syntax_error_logger);
        parser.ErrorHandler = semicolon_insertion_error_strategy;

        ast = parser.program();

        if (this.num_errors > 0) {
            return null;
        }

        /* 3. AST Compilation */
        ast_visitor = new JSCompilerASTVisitor(this);
        ast_result = ast_visitor.parseTree(ast);

        if (this.num_errors > 0) {
            /* On errors, discard code; it's invalid */
            return null;
        }

        /* Return code */
        ret = new JSCode();
        ret.code = ast_result.code.ToArray();
        // TODO: debuginfo

        return ret;
    }

    /**
     * Log a compilation message.
     */
    public void logCompilationMessage(CompilerMessage.Level level, string message, int line_number, int line_offset, string context) {
        CompilerMessage entry = new CompilerMessage();

        if (level == CompilerMessage.Level.ERROR || level == CompilerMessage.Level.FATAL) {
            this.num_errors++;
        }

		entry.level = level;
		entry.message = message;
		entry.line_number = line_number;
		entry.line_offset = line_offset;
		entry.offending_context = context;

		this.messages.Add(entry);
    }
}

}
