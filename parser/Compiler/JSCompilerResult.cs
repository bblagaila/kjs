/*
 *  JSCompilerResult.cs
 *
 *  Author:
 *       Bogdan Blagaila <blbogdan2000@gmail.com>
 *
 *  Copyright (c) 2014 Bogdan Blagaila
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

using System;
using System.Collections.Generic;

namespace kjs.parser.Compiler {

class JSCompilerResult {
	public List<byte> code = new List<byte>();

    /* Metadata */
	public bool constant = true;
	public int num_arguments = 0;
	public List<string> local_variables = new List<string>();

    public struct FlowControl {
        public enum Type {
            NONE = 0,
            BREAK,
            CONTINUE
        }

        public Type type;
        public int jump_offset_position;

        /* For error logging purposes */
        public Antlr4.Runtime.ParserRuleContext context;
    }
    public List<FlowControl> flow_control = new List<FlowControl>();

    public List<JSCompilerResult> hoisted_functions = new List<JSCompilerResult>();

    public JSCompilerResult case_expression = null;

	public bool isConstant() {
		return this.constant;
	}

	public void addLocalVariable(string identifier) {
		this.local_variables.Add(identifier);
	}

	public int addOpCode(OpCodes.OpCode opcode) {
		int ret;

		ret = this.code.Count;
		this.code.Add((byte)opcode);

		return ret;
	}

	public int addInt32(Int32 number, int position = -1) {
		int ret;
		byte[] data;

		ret = this.code.Count;
		data = BitConverter.GetBytes(number);

        if (position == -1) {
		    this.code.AddRange(data);
        }
        else {
            for (int i = 0; i < sizeof(Int32); i++) {
                this.code[position + i] = data[i];
            }
        }

		return ret;
	}

	public int addFloat(float number) {
		int ret;
		byte[] data;

		ret = this.code.Count;
		data = BitConverter.GetBytes(number);

		this.code.AddRange(data);

		return ret;
	}

	public int addString(string text) {
		int ret;
		byte[] data;

		ret = this.code.Count;
		data = System.Text.Encoding.UTF8.GetBytes(text);

		/* TODO: Must handle NULL bytes in strings ... */
		this.code.AddRange(data);
		this.code.Add(0);

		return ret;
	}

	public void append(JSCompilerResult other, bool propagate_constant = false) {
        int code_old_length = this.code.Count;

        /* Merge code */
		this.code.AddRange(other.code);

        /* Should propagate constant flag ? */
		if (propagate_constant == true && other.constant != true) {
			this.constant = false;
		}

        /* Propagate local variables */
		this.local_variables.AddRange(other.local_variables);

        /* Propagate flow control markers */
        foreach (var flow_control in other.flow_control) {
            FlowControl new_flow_control;

            new_flow_control.type = flow_control.type;
            new_flow_control.jump_offset_position = code_old_length + flow_control.jump_offset_position;
            new_flow_control.context = flow_control.context;

            this.flow_control.Add(new_flow_control);
        }

        /* Propagate hoisted functions */
        this.hoisted_functions.AddRange(other.hoisted_functions);
	}

    public override string ToString() {
        string ret = "CompilerResult\n";
        OpCodes op = new OpCodes();

        ret += "BEGIN\n";
        ret += OpCodes.dumpCode(this.code.ToArray());
        ret += "END\n";

        return ret;
    }
}

}
