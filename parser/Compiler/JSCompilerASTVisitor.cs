﻿/*
 *  JSCompilerASTVisitor.cs
 *
 *  Author:
 *       Bogdan Blagaila <blbogdan2000@gmail.com>
 *
 *  Copyright (c) 2014 Bogdan Blagaila
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

using System;
using System.Collections.Generic;

namespace kjs.parser.Compiler {

class JSCompilerASTVisitor : JSGrammarBaseVisitor<JSCompilerResult> {
    protected JSCompiler compiler = null;

    /** Identifier inside catch(e) block that match the exception's name
     *  will be compiled to PUSH_EXCEPTION instead of PUSH_IDENTIFIER.
     *  This stack keeps track of those identifiers' names.
     */
    protected Stack<string> catchExceptionNameStack = null;

	/** Exceptions */
	public class FatalCompilationTermination : Exception {
	};

	/**
	 * Default constructor
	 */
	public JSCompilerASTVisitor(JSCompiler compiler) {
        this.compiler = compiler;
	}

	/**
	 * Evaluates the given AST and returns the compiled byte code
	 */
    public JSCompilerResult parseTree(Antlr4.Runtime.Tree.IParseTree tree) {
        JSCompilerResult ret = null;

        this.catchExceptionNameStack = new Stack<string>();

        ret = this.Visit(tree);

        return ret;
    }

	/** Utility functions for logging compilation messages (warnings, errors) */

	public void logCompilerMessage(JSCompiler.CompilerMessage.Level level, Antlr4.Runtime.ParserRuleContext offending_context, string message) {
        this.compiler.logCompilationMessage(level, message, offending_context.Start.Line, offending_context.Start.Column, offending_context.GetText());

		if (level == JSCompiler.CompilerMessage.Level.FATAL) {
			throw new FatalCompilationTermination();
		}
	}

	public void logWarning(Antlr4.Runtime.ParserRuleContext offending_context, string message) {
		this.logCompilerMessage(JSCompiler.CompilerMessage.Level.ERROR, offending_context, message);
	}

	public void logError(Antlr4.Runtime.ParserRuleContext offending_context, string message) {
		this.logCompilerMessage(JSCompiler.CompilerMessage.Level.ERROR, offending_context, message);
	}

	public void logFatal(Antlr4.Runtime.ParserRuleContext offending_context, string message) {
		this.logCompilerMessage(JSCompiler.CompilerMessage.Level.FATAL, offending_context, message);
	}

	/** Utility functions for converting tokens to literal values */

	public float tokenToNumber(string text) {
		float value;

		if (float.TryParse(text, out value) == false) {
			value = float.NaN;
		}

		return value;
	}

	public string tokenToString(string text) {
		string value;

		/* Remove quotes */
		value = text;
		value = value.Substring(1, value.Length - 2);

		/* TODO: Parse escape sequences */

		return value;
	}

	/** Override VisitChildren result agregator */
	protected override JSCompilerResult DefaultResult {
		get {
			return new JSCompilerResult();
		}
	}

	protected override JSCompilerResult AggregateResult(JSCompilerResult aggregate, JSCompilerResult nextResult) {
		aggregate.append(nextResult, true);
		return aggregate;
	}

    /* Used for functions and global scope; hoists functions, expects empty
     * flow control lists, etc.
     * Returns a new normalized result.
     */
    public JSCompilerResult normalizeScope(JSCompilerResult input) {
        JSCompilerResult ret = new JSCompilerResult();

        /* Local variables are propagated */

        /* End of scope; log errors if orphan 'break' or 'continue' found */
        foreach (var flow_control in input.flow_control) {
            if (flow_control.type == JSCompilerResult.FlowControl.Type.BREAK) {
                this.logError(flow_control.context, "Illegal break statement");
            }
            else if (flow_control.type == JSCompilerResult.FlowControl.Type.CONTINUE) {
                this.logError(flow_control.context, "Illegal continue statement");
            }
        }
        input.flow_control.Clear();

        /* Prepend hoisted functions */
        foreach (var function in input.hoisted_functions) {
            ret.append(function);
        }
        input.hoisted_functions.Clear();

        ret.append(input);

        return ret;
    }

	/**
	 * Utility method for a left-hand size reference expression. Throws compiler error is the expression is
	 * constant. Used by assignment operations below.
	 */
	protected void handleReferenceExpression(JSCompilerResult result, Antlr4.Runtime.ParserRuleContext context) {
		JSCompilerResult r1 = this.Visit(context);

		result.append(r1);
		if (r1.isConstant() == true) {
			this.logError(context, "Invalid left-hand size expression in assignment");
		}
	}

	/**
	 * Utility method for unary expression operations. Considers the expression to be value not reference,
	 * so it is dereferenced if not. Used in mathematical operators below.
	 * Parses children contexts and appends their opcodes to the current result, adding DEREF if needed.
	 * When executed, the expression's value will be on stack.
	 */
	protected void handleUnaryExpression(JSCompilerResult result, Antlr4.Runtime.ParserRuleContext context) {
		JSCompilerResult r1 = this.Visit(context);

		result.append(r1);
		if (r1.isConstant() == false) {
			result.addOpCode(OpCodes.OpCode.DEREF);
		}
	}

	/**
	 * Utility method for binary expression operations. Considers both expressions to be values not references,
	 * so they are dereferenced if not. Used in mathematical operators below.
	 * Parses children contexts and appends their opcodes to the current result, adding DEREF if needed.
	 * When executed, the expression's value will be on stack, one after another.
	 */
	protected void handleBinaryExpression(JSCompilerResult result, Antlr4.Runtime.ParserRuleContext context1,
											Antlr4.Runtime.ParserRuleContext context2) {
		this.handleUnaryExpression(result, context1);
		this.handleUnaryExpression(result, context2);
	}

    /**
     * Utility method for building the body of a function.
     */
    protected JSCompilerResult buildFunction(string name,
                                                JSGrammarParser.FunctionFormalParametersContext formal_parameters,
                                                JSGrammarParser.StatementsContext statements,
                                                bool hoisted) {
        JSCompilerResult ret = new JSCompilerResult();
        JSCompilerResult function = new JSCompilerResult();
        JSCompilerResult body = null;

        function.addString(name);

        if (formal_parameters != null) {
            JSCompilerResult r = this.Visit(formal_parameters);

            function.addInt32(r.local_variables.Count);
            foreach (var identifier in r.local_variables) {
                function.addString(identifier);
            }
        }
        else {
            function.addInt32(0);
        }

        body = this.Visit(statements);

        body = this.normalizeScope(body);

        function.addInt32(body.local_variables.Count);
        foreach (var identifier in body.local_variables) {
            function.addString(identifier);
        }
        body.local_variables.Clear();

        function.append(body);

        /* Just to be sure that it returns if the body has no 'return' statement */
        /* TODO: optimize this */
        function.addOpCode(OpCodes.OpCode.PUSH_UNDEFINED);
        function.addOpCode(OpCodes.OpCode.RET);

        /* Build function */
        if (hoisted) {
            ret.addOpCode(OpCodes.OpCode.PUSH_FUNCTION_HOISTED);
        }
        else {
            ret.addOpCode(OpCodes.OpCode.PUSH_FUNCTION);
        }
        ret.addInt32(function.code.Count);
        ret.append(function);

        return ret;
    }

    /** AST Visitors */

    public override JSCompilerResult VisitProgram(JSGrammarParser.ProgramContext context) {
        JSCompilerResult ret = new JSCompilerResult();
        JSCompilerResult body = null;

        body = this.normalizeScope(this.VisitChildren(context));

        foreach (var identifier in body.local_variables) {
            ret.addOpCode(OpCodes.OpCode.PUSH_IDENTIFIER);
            ret.addString(identifier);
            ret.addOpCode(OpCodes.OpCode.PUSH_UNDEFINED);
            ret.addOpCode(OpCodes.OpCode.ASSIGN);
            ret.addOpCode(OpCodes.OpCode.POP);
        }

        ret.append(body);

        return ret;
    }

    public override JSCompilerResult VisitStatement(JSGrammarParser.StatementContext context) {
        JSCompilerResult ret = null;
        
        if (context.BREAK_KEYWORD() != null || context.CONTINUE_KEYWORD() != null) {
            JSCompilerResult.FlowControl flow_control;
            ret = new JSCompilerResult();

            flow_control.context = context;

            if (context.BREAK_KEYWORD() != null) {
                flow_control.type = JSCompilerResult.FlowControl.Type.BREAK;
            }
            else {
                flow_control.type = JSCompilerResult.FlowControl.Type.CONTINUE;
            }

            ret.addOpCode(OpCodes.OpCode.JMP);
            flow_control.jump_offset_position = ret.addInt32(0);

            ret.flow_control.Add(flow_control);
            
            return ret;
        }

        return base.VisitStatement(context);
    }

    public override JSCompilerResult VisitTryCatchConstruct(JSGrammarParser.TryCatchConstructContext context) {
        JSCompilerResult ret = new JSCompilerResult();
        JSCompilerResult try_scope = null;
        JSCompilerResult catch_scope = null;
        string caught_exception_name = "";

        if (context.IDENTIFIER() != null) {
            caught_exception_name = context.IDENTIFIER().GetText();
        }

        try_scope = this.Visit(context.try_scope);

        this.catchExceptionNameStack.Push(caught_exception_name);
        catch_scope = this.Visit(context.catch_scope);
        this.catchExceptionNameStack.Pop();

        try_scope.addOpCode(OpCodes.OpCode.JMP);
        try_scope.addInt32(catch_scope.code.Count);

        ret.addOpCode(OpCodes.OpCode.TRY_BEGIN);

        ret.addInt32(try_scope.code.Count);

        ret.append(try_scope);

        ret.append(catch_scope);

        ret.addOpCode(OpCodes.OpCode.TRY_END);

        return ret;
    }

    public override JSCompilerResult VisitThrowConstruct(JSGrammarParser.ThrowConstructContext context) {
        JSCompilerResult ret = new JSCompilerResult();
        JSCompilerResult expression = null;

        expression = this.Visit(context.expression());
        if (expression.isConstant() == false) {
            expression.addOpCode(OpCodes.OpCode.DEREF);
        }

        ret.append(expression);
        ret.addOpCode(OpCodes.OpCode.THROW);

        return ret;
    }

    public override JSCompilerResult VisitReturnConstruct(JSGrammarParser.ReturnConstructContext context) {
        JSCompilerResult ret = new JSCompilerResult();

        if (context.expression() != null) {
            JSCompilerResult r = this.Visit(context.expression());
            if (r.isConstant() == false) {
                r.addOpCode(OpCodes.OpCode.DEREF);
            }
            ret.append(r);
        }
        else {
            ret.addOpCode(OpCodes.OpCode.PUSH_UNDEFINED);
        }

        ret.addOpCode(OpCodes.OpCode.RET);

        return ret;
    }

    public override JSCompilerResult VisitWhileConstruct(JSGrammarParser.WhileConstructContext context) {
        JSCompilerResult ret = new JSCompilerResult();
        JSCompilerResult condition = null;
        JSCompilerResult body = null;
        int body_size = 0;

        body = new JSCompilerResult();
        body_size += (1 + sizeof(Int32)); /* JMP [offset] */

        if (context.block() != null) {
            JSCompilerResult r = this.Visit(context.block());
            body_size += r.code.Count;
            body.append(r);
        }

        condition = this.VisitChildren(context.condition);
        if (condition.isConstant() == false) {
            condition.addOpCode(OpCodes.OpCode.DEREF);
        }
        condition.addOpCode(OpCodes.OpCode.JF);
        condition.addInt32(body_size);

        body.addOpCode(OpCodes.OpCode.JMP);
        body.addInt32(-(body_size + condition.code.Count));

        /* Handle "continue" and "break" */
        foreach (var flow_control in body.flow_control) {
            int jmp_offset = 0;

            if (flow_control.type == JSCompilerResult.FlowControl.Type.BREAK) {
                jmp_offset -= (flow_control.jump_offset_position + sizeof(Int32));
                jmp_offset += body_size;
            }
            else if (flow_control.type == JSCompilerResult.FlowControl.Type.CONTINUE) {
                jmp_offset -= (flow_control.jump_offset_position + sizeof(Int32));
                jmp_offset -= condition.code.Count;
            }

            body.addInt32(jmp_offset, flow_control.jump_offset_position);
        }
        body.flow_control.Clear();

        ret.append(condition);
        ret.append(body);

        return ret;
    }

    public override JSCompilerResult VisitDoWhileConstruct(JSGrammarParser.DoWhileConstructContext context) {
        JSCompilerResult ret = new JSCompilerResult();
        JSCompilerResult body = null;
        JSCompilerResult condition = null;

        body = this.Visit(context.statements());
        condition = this.VisitChildren(context.condition);

        /* Handle continue and break */
        foreach (var flow_control in body.flow_control) {
            int jmp_offset = 0;

            if (flow_control.type == JSCompilerResult.FlowControl.Type.BREAK) {
                jmp_offset -= (flow_control.jump_offset_position + sizeof(Int32));
                jmp_offset += (body.code.Count + condition.code.Count + 1 + sizeof(Int32));
            }
            else if (flow_control.type == JSCompilerResult.FlowControl.Type.CONTINUE) {
                jmp_offset -= (flow_control.jump_offset_position + sizeof(Int32));
                jmp_offset += body.code.Count;
            }

            body.addInt32(jmp_offset, flow_control.jump_offset_position);
        }
        body.flow_control.Clear();

        ret.append(body);
        ret.append(condition);
        ret.addOpCode(OpCodes.OpCode.JT);
        ret.addInt32(-(body.code.Count + condition.code.Count + 1 + sizeof(Int32)));

        return ret;
    }

    public override JSCompilerResult VisitForConstruct(JSGrammarParser.ForConstructContext context) {
		JSCompilerResult ret = new JSCompilerResult();
		JSCompilerResult initialization = null;
		JSCompilerResult condition = null;
		JSCompilerResult body = null;
        int block_size = 0;
		int body_size = 0;

		/* Initialization */
		if (context.variablesDeclaration() != null) {
			initialization = this.Visit(context.variablesDeclaration());
			ret.append(initialization);
		}
		else if (context.initialization != null) {
			initialization = this.VisitChildren(context.initialization);
			ret.append(initialization);
		}

		/* Structure:
		 *  [ initialization ]
		 *  <condition>
		 * 	[ condition JF <to_end> ]
		 *  [ block [ continue <to_condition> | break <to_end> ] ]
		 *  [ post operation ]
		 *  [ JMP <to_condition> ]
		 *  <end>
		 */

        body = new JSCompilerResult();
        condition = new JSCompilerResult();

        if (context.block() != null) {
            JSCompilerResult r = this.Visit(context.block());
            block_size = r.code.Count;
            body.append(r);
        }

        if (context.postoperation != null) {
            JSCompilerResult r = this.VisitChildren(context.postoperation);
            body.append(r);
        }

		body_size = (body.code.Count) + (1 + sizeof(Int32));

		if (context.condition != null) {
			JSCompilerResult r1 = this.VisitChildren(context.condition);
            if (r1.isConstant() == false) {
                r1.addOpCode(OpCodes.OpCode.DEREF);
            }

			condition.append(r1);
            condition.addOpCode(OpCodes.OpCode.JF);
            condition.addInt32(body_size);
		}

        body.addOpCode(OpCodes.OpCode.JMP);
        body.addInt32(-(body_size + condition.code.Count));

        /* Handle break and continue */
        foreach (var flow_control in body.flow_control) {
            int jmp_offset = 0;

            if (flow_control.type == JSCompilerResult.FlowControl.Type.BREAK) {
                jmp_offset -= (flow_control.jump_offset_position + sizeof(Int32));
                jmp_offset += body_size;
            }
            else if (flow_control.type == JSCompilerResult.FlowControl.Type.CONTINUE) {
                jmp_offset -= (flow_control.jump_offset_position + sizeof(Int32));
                jmp_offset += block_size;
            }

            body.addInt32(jmp_offset, flow_control.jump_offset_position);
        }
        body.flow_control.Clear();

        ret.append(condition);
        ret.append(body);

		return ret;
	}

    public override JSCompilerResult VisitSwitchCaseConstruct(JSGrammarParser.SwitchCaseConstructContext context) {
        JSCompilerResult ret = new JSCompilerResult();

        ret.append(this.Visit(context.statements()));

        if (context.CASE_KEYWORD() != null) {
            ret.case_expression = this.Visit(context.expression());
            if (ret.case_expression.isConstant() == false) {
                ret.case_expression.addOpCode(OpCodes.OpCode.DEREF);
            }
        }
        else {
            ret.case_expression = null;
        }

        return ret;
    }

    public override JSCompilerResult VisitSwitchConstruct(JSGrammarParser.SwitchConstructContext context) {
        JSCompilerResult ret = new JSCompilerResult();
        JSCompilerResult candidate = null;
        JSCompilerResult conditions = new JSCompilerResult();
        JSCompilerResult default_case = null;
        int default_case_body_offset = -1;
        JSCompilerResult body = new JSCompilerResult();
        JSGrammarParser.SwitchCaseConstructContext[] cases = null;
        List< KeyValuePair<int, int> > case_jump_offsets = new List< KeyValuePair<int, int> >();

        /**
         * Structure:
         * initial: [expression]
         * conditions (cases):
         *      DUP, <expression>, EQ, JT <to_body_offset>
         *      DUP, <expression>, EQ, JT <to_body_offset>
         *      ...
         *      JMP <to_body_default|to_end>
         * body:
         *      <code1>, JMP <to_end>
         *      <code2>, // fallthrough
         *      <code3>, JMP <to_end>
         *      <default_code>
         * end:
         */
        
        candidate = this.Visit(context.expression());
        if (candidate.isConstant() == false) {
            candidate.addOpCode(OpCodes.OpCode.DEREF);
        }

        ret.append(candidate);

        cases = context.switchCaseConstruct();

        foreach (var case_entry in cases) {
            int jump_offset = 0, jump_to_body_offset = 0;
            JSCompilerResult r = this.Visit(case_entry);

            if (r.case_expression == null) {
                if (default_case != null) {
                    this.logError(case_entry, "More than one default clause in switch statement");
                    continue;
                }

                default_case = r;
                default_case_body_offset = body.code.Count;
                body.append(r);
                continue;
            }

            jump_to_body_offset = body.code.Count;
            body.append(r);

            conditions.addOpCode(OpCodes.OpCode.DUP);
            conditions.append(r.case_expression);
            conditions.addOpCode(OpCodes.OpCode.IS_EQ);
            conditions.addOpCode(OpCodes.OpCode.JT);
            jump_offset = conditions.addInt32(0);

            case_jump_offsets.Add(new KeyValuePair<int, int>(jump_offset, jump_to_body_offset));
        }

        if (default_case != null) {
            int jump_offset;

            if (default_case_body_offset > 0) {
                conditions.addOpCode(OpCodes.OpCode.JMP);
                jump_offset = conditions.addInt32(0);

                case_jump_offsets.Add(new KeyValuePair<int, int>(jump_offset, default_case_body_offset));
            }
        }
        else {
            conditions.addOpCode(OpCodes.OpCode.JMP);
            conditions.addInt32(body.code.Count);
        }

        /* Resolve conditions' jumps */
        foreach (var entry in case_jump_offsets) {
            int jmp_offset = 0;

            jmp_offset -= entry.Key + sizeof(Int32);
            jmp_offset += conditions.code.Count;
            jmp_offset += entry.Value;

            conditions.addInt32(jmp_offset, entry.Key);
        }

        ret.append(conditions);

        /* Append body and resolve 'breaks' */
        List<JSCompilerResult.FlowControl> others = new List<JSCompilerResult.FlowControl>();
        foreach (var entry in body.flow_control) {
            int jump_offset = 0;

            jump_offset -= entry.jump_offset_position + sizeof(Int32);

            if (entry.type == JSCompilerResult.FlowControl.Type.BREAK) {
                jump_offset += body.code.Count;
            }
            else {
                others.Add(entry);
                continue;
            }

            body.addInt32(jump_offset, entry.jump_offset_position);
        }
        body.flow_control.Clear();
        body.flow_control.AddRange(others);

        ret.append(body);

        return ret;
    }

    public override JSCompilerResult VisitIfConstruct(JSGrammarParser.IfConstructContext context) {
		JSCompilerResult ret = new JSCompilerResult();
		JSCompilerResult cond = this.VisitChildren(context.condition);
		JSCompilerResult then = null;
		JSCompilerResult otherwise = null;

		ret.append(cond);
		if (cond.isConstant() == false) {
			ret.addOpCode(OpCodes.OpCode.DEREF);
		}

		if (context.then != null) {
			then = this.VisitChildren(context.then);

			if (context.otherwise != null) {
				otherwise = this.VisitChildren(context.otherwise);

				/* If we have an "else" section, the "then" section must jump over it */
				then.addOpCode(OpCodes.OpCode.JMP);
				then.addInt32(otherwise.code.Count);
			}

			ret.addOpCode(OpCodes.OpCode.JF);
			ret.addInt32(then.code.Count);

			ret.append(then);

			if (otherwise != null) {
				ret.append(otherwise);
			}
		}

		return ret;
	}

	public override JSCompilerResult VisitVarDeclaration(JSGrammarParser.VarDeclarationContext context) {
		JSCompilerResult ret = new JSCompilerResult();
		string identifier = context.IDENTIFIER().GetText();

		/* Add identifier to local variables list */
		ret.addLocalVariable(identifier);

		/* Do we have an initial value assigned ? */
		if (context.expression() != null) {
			ret.addOpCode(OpCodes.OpCode.PUSH_IDENTIFIER);
			ret.addString(identifier);

			this.handleUnaryExpression(ret, context.expression());

			ret.addOpCode(OpCodes.OpCode.ASSIGN);
		}

		return ret;
	}

	public override JSCompilerResult VisitVariablesDeclaration(JSGrammarParser.VariablesDeclarationContext context) {
		return this.VisitChildren(context);
	}

	public override JSCompilerResult VisitAssignmentExpression(JSGrammarParser.AssignmentExpressionContext context) {
		JSCompilerResult ret = new JSCompilerResult();

		if (context.ASSIGN_OPERATOR() != null) {
			this.handleReferenceExpression(ret, context.conditionalExpression());
			this.handleUnaryExpression(ret, context.assignmentExpression());
			ret.addOpCode(OpCodes.OpCode.ASSIGN);
		}
		else if (context.MULT_ASSIGN_OPERATOR() != null || context.DIV_ASSIGN_OPERATOR() != null || context.REM_ASSIGN_OPERATOR() != null) {
			this.handleReferenceExpression(ret, context.conditionalExpression());
			ret.addOpCode(OpCodes.OpCode.DUP);
			ret.addOpCode(OpCodes.OpCode.DEREF);
			this.handleUnaryExpression(ret, context.assignmentExpression());

			if (context.MULT_ASSIGN_OPERATOR() != null) {
				ret.addOpCode(OpCodes.OpCode.MULT);
			}
			else if (context.DIV_ASSIGN_OPERATOR() != null) {
				ret.addOpCode(OpCodes.OpCode.DIV);
			}
			else if (context.REM_ASSIGN_OPERATOR() != null) {
				ret.addOpCode(OpCodes.OpCode.REM);
			}

			ret.addOpCode(OpCodes.OpCode.ASSIGN);
		}
		else if (context.ADD_ASSIGN_OPERATOR() != null || context.SUB_ASSIGN_OPERATOR() != null) {
			this.handleReferenceExpression(ret, context.conditionalExpression());
			ret.addOpCode(OpCodes.OpCode.DUP);
			ret.addOpCode(OpCodes.OpCode.DEREF);
			this.handleUnaryExpression(ret, context.assignmentExpression());

			if (context.ADD_ASSIGN_OPERATOR() != null) {
				ret.addOpCode(OpCodes.OpCode.ADD);
			}
			else if (context.SUB_ASSIGN_OPERATOR() != null) {
				ret.addOpCode(OpCodes.OpCode.SUB);
			}

			ret.addOpCode(OpCodes.OpCode.ASSIGN);
		}
		else if (context.LSHIFT_ASSIGN_OPERATOR() != null || context.RSHIFT_ASSIGN_OPERATOR() != null || context.ZFRSHIFT_ASSIGN_OPERATOR() != null) {
			this.handleReferenceExpression(ret, context.conditionalExpression());
			ret.addOpCode(OpCodes.OpCode.DUP);
			ret.addOpCode(OpCodes.OpCode.DEREF);
			this.handleUnaryExpression(ret, context.assignmentExpression());

			if (context.LSHIFT_ASSIGN_OPERATOR() != null) {
				ret.addOpCode(OpCodes.OpCode.LSHIFT);
			}
			else if (context.RSHIFT_ASSIGN_OPERATOR() != null) {
				ret.addOpCode(OpCodes.OpCode.RSHIFT);
			}
			else if (context.ZFRSHIFT_ASSIGN_OPERATOR() != null) {
				ret.addOpCode(OpCodes.OpCode.ZFRSHIFT);
			}

			ret.addOpCode(OpCodes.OpCode.ASSIGN);
		}
		else if (context.AND_ASSIGN_OPERATOR() != null || context.OR_ASSIGN_OPERATOR() != null || context.XOR_ASSIGN_OPERATOR() != null) {
			this.handleReferenceExpression(ret, context.conditionalExpression());
			ret.addOpCode(OpCodes.OpCode.DUP);
			ret.addOpCode(OpCodes.OpCode.DEREF);
			this.handleUnaryExpression(ret, context.assignmentExpression());

			if (context.AND_ASSIGN_OPERATOR() != null) {
				ret.addOpCode(OpCodes.OpCode.B_AND);
			}
			else if (context.OR_ASSIGN_OPERATOR() != null) {
				ret.addOpCode(OpCodes.OpCode.B_OR);
			}
			else if (context.XOR_ASSIGN_OPERATOR() != null) {
				ret.addOpCode(OpCodes.OpCode.B_XOR);
			}

			ret.addOpCode(OpCodes.OpCode.ASSIGN);
		}
		else {
			return this.Visit(context.conditionalExpression());
		}

		return ret;
	}

	public override JSCompilerResult VisitConditionalExpression(JSGrammarParser.ConditionalExpressionContext context) {
		JSCompilerResult ret = new JSCompilerResult();

		if (context.COND_OPERATOR() != null) {
			JSCompilerResult cond = this.Visit(context.logicalOrExpression());
			JSCompilerResult then = this.VisitChildren(context.then);
			JSCompilerResult otherwise = this.VisitChildren(context.otherwise);

			/* The result of this expressions will be the dereferenced value of the
			 * candidate expressions */
			if (then.isConstant() == false) {
				then.addOpCode(OpCodes.OpCode.DEREF);
			}
			if (otherwise.isConstant() == false) {
				otherwise.addOpCode(OpCodes.OpCode.DEREF);
			}

			/* Add JMP over the "else" section at the end of the "then" section */
			then.addOpCode(OpCodes.OpCode.JMP);
			then.addInt32(otherwise.code.Count);

			/* Add condition expression first */
			ret.append(cond);
			if (cond.isConstant() == false) {
				ret.addOpCode(OpCodes.OpCode.DEREF);
			}

			/* Jump to the "else" section if the condition is false;
			 * otherwise fall-through to the "then" section */
			ret.addOpCode(OpCodes.OpCode.JF);
			ret.addInt32(then.code.Count);

			/* Add "then" section */
			ret.append(then);

			/* Add "else" section */
			ret.append(otherwise);
		}
		else {
			return this.Visit(context.logicalOrExpression());
		}

		return ret;
	}

	public override JSCompilerResult VisitLogicalOrExpression(JSGrammarParser.LogicalOrExpressionContext context) {
		JSCompilerResult ret = new JSCompilerResult();

		if (context.L_OR_OPERATOR() != null) {
			this.handleBinaryExpression(ret, context.logicalAndExpression(), context.logicalOrExpression());
			ret.addOpCode(OpCodes.OpCode.L_OR);
		}
		else {
			return this.Visit(context.logicalAndExpression());
		}

		return ret;
	}

	public override JSCompilerResult VisitLogicalAndExpression(JSGrammarParser.LogicalAndExpressionContext context) {
		JSCompilerResult ret = new JSCompilerResult();

		if (context.L_AND_OPERATOR() != null) {
			this.handleBinaryExpression(ret, context.bitwiseOrExpression(), context.logicalAndExpression());
			ret.addOpCode(OpCodes.OpCode.L_AND);
		}
		else {
			return this.Visit(context.bitwiseOrExpression());
		}

		return ret;
	}

	public override JSCompilerResult VisitBitwiseOrExpression(JSGrammarParser.BitwiseOrExpressionContext context) {
		JSCompilerResult ret = new JSCompilerResult();

		if (context.B_OR_OPERATOR() != null) {
			this.handleBinaryExpression(ret, context.bitwiseXorExpression(), context.bitwiseOrExpression());
			ret.addOpCode(OpCodes.OpCode.B_OR);
		}
		else {
			return this.Visit(context.bitwiseXorExpression());
		}

		return ret;
	}

	public override JSCompilerResult VisitBitwiseXorExpression(JSGrammarParser.BitwiseXorExpressionContext context) {
		JSCompilerResult ret = new JSCompilerResult();

		if (context.B_XOR_OPERATOR() != null) {
			this.handleBinaryExpression(ret, context.bitwiseAndExpression(), context.bitwiseXorExpression());
			ret.addOpCode(OpCodes.OpCode.B_XOR);
		}
		else {
			return this.Visit(context.bitwiseAndExpression());
		}

		return ret;
	}

	public override JSCompilerResult VisitBitwiseAndExpression(JSGrammarParser.BitwiseAndExpressionContext context) {
		JSCompilerResult ret = new JSCompilerResult();

		if (context.B_AND_OPERATOR() != null) {
			this.handleBinaryExpression(ret, context.equalityExpression(), context.bitwiseAndExpression());
			ret.addOpCode(OpCodes.OpCode.B_AND);
		}
		else {
			return this.Visit(context.equalityExpression());
		}

		return ret;
	}

	public override JSCompilerResult VisitEqualityExpression(JSGrammarParser.EqualityExpressionContext context) {
		JSCompilerResult ret = new JSCompilerResult();

		if (context.EQ_OPERATOR() != null) {
			this.handleBinaryExpression(ret, context.relationalExpression(), context.equalityExpression());
			ret.addOpCode(OpCodes.OpCode.IS_EQ);
		}
		else if (context.NEQ_OPERATOR() != null) {
			this.handleBinaryExpression(ret, context.relationalExpression(), context.equalityExpression());
			ret.addOpCode(OpCodes.OpCode.IS_NEQ);
		}
		else if (context.SEQ_OPERATOR() != null) {
			this.handleBinaryExpression(ret, context.relationalExpression(), context.equalityExpression());
			ret.addOpCode(OpCodes.OpCode.IS_SEQ);
		}
		else if (context.NSEQ_OPERATOR() != null) {
			this.handleBinaryExpression(ret, context.relationalExpression(), context.equalityExpression());
			ret.addOpCode(OpCodes.OpCode.IS_NSEQ);
		}
		else {
			return this.Visit(context.relationalExpression());
		}

		return ret;
	}

	public override JSCompilerResult VisitRelationalExpression(JSGrammarParser.RelationalExpressionContext context) {
		JSCompilerResult ret = new JSCompilerResult();

		if (context.LT_OPERATOR() != null) {
			this.handleBinaryExpression(ret, context.shiftExpression(), context.relationalExpression());
			ret.addOpCode(OpCodes.OpCode.IS_LT);
		}
		else if (context.LTE_OPERATOR() != null) {
			this.handleBinaryExpression(ret, context.shiftExpression(), context.relationalExpression());
			ret.addOpCode(OpCodes.OpCode.IS_LTE);
		}
		else if (context.GT_OPERATOR() != null) {
			this.handleBinaryExpression(ret, context.shiftExpression(), context.relationalExpression());
			ret.addOpCode(OpCodes.OpCode.IS_GT);
		}
		else if (context.GTE_OPERATOR() != null) {
			this.handleBinaryExpression(ret, context.shiftExpression(), context.relationalExpression());
			ret.addOpCode(OpCodes.OpCode.IS_GTE);
		}
		else if (context.INSTANCEOF_KEYWORD() != null) {
			this.handleBinaryExpression(ret, context.shiftExpression(), context.relationalExpression());
			ret.addOpCode(OpCodes.OpCode.INSTANCEOF);
		}
		else if (context.IN_KEYWORD() != null) {
			this.handleBinaryExpression(ret, context.shiftExpression(), context.relationalExpression());
			ret.addOpCode(OpCodes.OpCode.IN);
		}
		else {
			return this.Visit(context.shiftExpression());
		}

		return ret;
	}

	public override JSCompilerResult VisitShiftExpression(JSGrammarParser.ShiftExpressionContext context) {
		JSCompilerResult ret = new JSCompilerResult();

		if (context.LSHIFT_OPERATOR() != null) {
			this.handleBinaryExpression(ret, context.additiveExpression(), context.shiftExpression());
			ret.addOpCode(OpCodes.OpCode.LSHIFT);
		}
		else if (context.RSHIFT_OPERATOR() != null) {
			this.handleBinaryExpression(ret, context.additiveExpression(), context.shiftExpression());
			ret.addOpCode(OpCodes.OpCode.RSHIFT);
		}
		else if (context.ZFRSHIFT_OPERATOR() != null) {
			this.handleBinaryExpression(ret, context.additiveExpression(), context.shiftExpression());
			ret.addOpCode(OpCodes.OpCode.ZFRSHIFT);
		}
		else {
			return this.Visit(context.additiveExpression());
		}

		return ret;
	}

	public override JSCompilerResult VisitAdditiveExpression(JSGrammarParser.AdditiveExpressionContext context) {
		JSCompilerResult ret = new JSCompilerResult();

		if (context.PLUS_OPERATOR() != null) {
			this.handleBinaryExpression(ret, context.multiplicativeExpression(), context.additiveExpression());
			ret.addOpCode(OpCodes.OpCode.ADD);
		}
		else if (context.MINUS_OPERATOR() != null) {
			this.handleBinaryExpression(ret, context.multiplicativeExpression(), context.additiveExpression());
			ret.addOpCode(OpCodes.OpCode.SUB);
		}
		else {
			return this.Visit(context.multiplicativeExpression());
		}

		return ret;
	}

	public override JSCompilerResult VisitMultiplicativeExpression(JSGrammarParser.MultiplicativeExpressionContext context) {
		JSCompilerResult ret = new JSCompilerResult();

		if (context.MULT_OPERATOR() != null) {
			this.handleBinaryExpression(ret, context.unaryExpression(), context.multiplicativeExpression());
			ret.addOpCode(OpCodes.OpCode.MULT);
		}
		else if (context.DIV_OPERATOR() != null) {
			this.handleBinaryExpression(ret, context.unaryExpression(), context.multiplicativeExpression());
			ret.addOpCode(OpCodes.OpCode.DIV);
		}
		else if (context.REM_OPERATOR() != null) {
			this.handleBinaryExpression(ret, context.unaryExpression(), context.multiplicativeExpression());
			ret.addOpCode(OpCodes.OpCode.REM);
		}
		else {
			return this.Visit(context.unaryExpression());
		}

		return ret;
	}

	public override JSCompilerResult VisitUnaryExpression(JSGrammarParser.UnaryExpressionContext context) {
		JSCompilerResult ret = new JSCompilerResult();

		if (context.INCREMENT_OPERATOR() != null || context.DECREMENT_OPERATOR() != null) {
			JSCompilerResult r = this.Visit(context.unaryExpression());

			if (r.isConstant() == true) {
				this.logError(context, "Invalid left-hand size expression in prefix operation");
			}

			ret.append(r);
			ret.addOpCode(OpCodes.OpCode.DUP);
			ret.addOpCode(OpCodes.OpCode.DEREF);

			if (context.INCREMENT_OPERATOR() != null) {
				ret.addOpCode(OpCodes.OpCode.INC);
			}
			else {
				ret.addOpCode(OpCodes.OpCode.DEC);
			}

			ret.addOpCode(OpCodes.OpCode.ASSIGN);
		}
		else if (context.MINUS_OPERATOR() != null || context.PLUS_OPERATOR() != null) {
			JSCompilerResult r = this.Visit(context.unaryExpression());
			ret.append(r);
			if (r.isConstant() == false) {
				ret.addOpCode(OpCodes.OpCode.DEREF);
			}

			if (context.MINUS_OPERATOR() != null) {
				ret.addOpCode(OpCodes.OpCode.MINUS);
			}
			else {
				ret.addOpCode(OpCodes.OpCode.PLUS);
			}
		}
		else if (context.BITWISE_NOT_OPERATOR() != null || context.LOGICAL_NOT_OPERATOR() != null) {
			JSCompilerResult r = this.Visit(context.unaryExpression());
			ret.append(r);
			if (r.isConstant() == false) {
				ret.addOpCode(OpCodes.OpCode.DEREF);
			}

			if (context.BITWISE_NOT_OPERATOR() != null) {
				ret.addOpCode(OpCodes.OpCode.B_NOT);
			}
			else {
				ret.addOpCode(OpCodes.OpCode.L_NOT);
			}
		}
		else if (context.DELETE_KEYWORD() != null) {
			ret.append(this.Visit(context.unaryExpression()));
			ret.addOpCode(OpCodes.OpCode.DELETE);
		}
		else if (context.TYPEOF_KEYWORD() != null) {
			JSCompilerResult r = this.Visit(context.unaryExpression());
			ret.append(r);
			if (r.isConstant() == false) {
				ret.addOpCode(OpCodes.OpCode.DEREF);
			}

			ret.addOpCode(OpCodes.OpCode.TYPEOF);
		}
		else {
			return this.Visit(context.postfixExpression());
		}

		return ret;
	}

	public override JSCompilerResult VisitPostfixExpression(JSGrammarParser.PostfixExpressionContext context) {
		JSCompilerResult ret = new JSCompilerResult();

		if (context.INCREMENT_OPERATOR() != null || context.DECREMENT_OPERATOR() != null) {
			JSCompilerResult r = this.Visit(context.callExpression());

			if (r.isConstant() == true) {
				this.logError(context, "Invalid left-hand size expression in postfix operation");
			}

			ret.append(r);
			ret.addOpCode(OpCodes.OpCode.DEREF_DUP);
			ret.addOpCode(OpCodes.OpCode.DUPN);
			ret.addInt32(1);

			if (context.INCREMENT_OPERATOR() != null) {
				ret.addOpCode(OpCodes.OpCode.INC);
			}
			else {
				ret.addOpCode(OpCodes.OpCode.DEC);
			}

			ret.addOpCode(OpCodes.OpCode.ASSIGN);
			ret.addOpCode(OpCodes.OpCode.POP);
		}
		else {
			return this.Visit(context.callExpression());
		}

		return ret;
	}

	public override JSCompilerResult VisitFunctionCallParameters(JSGrammarParser.FunctionCallParametersContext context) {
		JSCompilerResult ret = new JSCompilerResult();
		JSGrammarParser.ExpressionContext[] expressions = context.expression();

		for (int i = 0; i < expressions.Length; i++) {
			JSCompilerResult r = this.Visit(expressions[i]);

			ret.append(r);
			if (r.isConstant() == false) {
				ret.addOpCode(OpCodes.OpCode.DEREF);
			}
		}

		ret.num_arguments = expressions.Length;

		return ret;
	}

	public override JSCompilerResult VisitCallExpression(JSGrammarParser.CallExpressionContext context) {
		JSCompilerResult ret = new JSCompilerResult();

		if (context.NEW_KEYWORD() != null) {
			int num_arguments = 0;

			/* Constructor expression */
			JSCompilerResult constructor = this.Visit(context.callExpression());

			ret.append(constructor);
			if (constructor.isConstant() == false) {
				ret.addOpCode(OpCodes.OpCode.DEREF);
			}

			if (context.functionCallParameters() != null) {
				JSCompilerResult arguments = this.Visit(context.functionCallParameters());
				num_arguments = arguments.num_arguments;
				ret.append(arguments);
			}

			ret.addOpCode(OpCodes.OpCode.NEW);
			ret.addInt32(num_arguments);
		}
		else if (context.OPEN_PARANTHESIS() != null) {
			int num_arguments = 0;

			/* Function call expression */
			JSCompilerResult function = this.Visit(context.callExpression());

            ret.append(function);
            if (function.isConstant() == true) {
                ret.addOpCode(OpCodes.OpCode.PUSH_NULL);
            }
            else {
                ret.addOpCode(OpCodes.OpCode.DUP);
                ret.addOpCode(OpCodes.OpCode.DEREF);
                // ret.addOpCode(OpCodes.OpCode.DEREF_PUSH_PARENT);
            }

			if (context.functionCallParameters() != null) {
				JSCompilerResult arguments = this.Visit(context.functionCallParameters());
				num_arguments = arguments.num_arguments;
				ret.append(arguments);
			}

			ret.addOpCode(OpCodes.OpCode.CALL);
			ret.addInt32(num_arguments);
		}
		else if (context.MEMBER_OPERATOR() != null) {
			JSCompilerResult parent = this.Visit(context.callExpression());

			ret.append(parent);
			if (parent.isConstant() == false) {
				ret.addOpCode(OpCodes.OpCode.DEREF);
			}

			ret.addOpCode(OpCodes.OpCode.PUSH_IDENTIFIER);
			ret.addString(context.IDENTIFIER().GetText());

			ret.addOpCode(OpCodes.OpCode.MEMBER_ID);

			ret.constant = false;
		}
		else if (context.OPEN_SQUARE_BRACKET() != null) {
			JSCompilerResult parent = this.Visit(context.callExpression());
			JSCompilerResult property = this.Visit(context.expression());

			ret.append(parent);
			if (parent.isConstant() == false) {
				ret.addOpCode(OpCodes.OpCode.DEREF);
			}

			ret.append(property);
			if (property.isConstant() == false) {
				ret.addOpCode(OpCodes.OpCode.DEREF);
			}

			ret.addOpCode(OpCodes.OpCode.MEMBER);

			ret.constant = false;
		}
		else {
			return this.Visit(context.expressionTerminal());
		}

		return ret;
	}

	public override JSCompilerResult VisitExpressionTerminal(JSGrammarParser.ExpressionTerminalContext context) {
		JSCompilerResult ret = new JSCompilerResult();

		if (context.UNDEFINED_LITERAL() != null) {
			ret.addOpCode(OpCodes.OpCode.PUSH_UNDEFINED);
		}
		else if (context.NULL_LITERAL() != null) {
			ret.addOpCode(OpCodes.OpCode.PUSH_NULL);
		}
		else if (context.BOOLEAN_LITERAL() != null) {
			if (context.BOOLEAN_LITERAL().GetText() == "true") {
				ret.addOpCode(OpCodes.OpCode.PUSH_BOOLEAN_TRUE);
			}
			else {
				ret.addOpCode(OpCodes.OpCode.PUSH_BOOLEAN_FALSE);
			}
		}
		else if (context.NUMERIC_LITERAL() != null) {
			ret.addOpCode(OpCodes.OpCode.PUSH_NUMBER);
			ret.addFloat(this.tokenToNumber(context.NUMERIC_LITERAL().GetText()));
		}
		else if (context.STRING_LITERAL() != null) {
			ret.addOpCode(OpCodes.OpCode.PUSH_STRING);
			ret.addString(this.tokenToString(context.STRING_LITERAL().GetText()));
		}
		else if (context.REGEX_LITERAL() != null) {
			/* TODO: Implement regex literal */
			this.logFatal(context, "Internal Error: Not implemented");
			throw new System.NotImplementedException();
		}
		else if (context.arrayLiteral() != null) {
			/* TODO: Implement array literal */
			this.logFatal(context, "Internal Error: Not implemented");
			throw new System.NotImplementedException();
		}
		else if (context.objectLiteral() != null) {
			ret.append(this.Visit(context.objectLiteral()));
		}
		else if (context.functionDeclarationInExpression() != null) {
			ret.append(this.Visit(context.functionDeclarationInExpression()));
		}
		else if (context.THIS_KEYWORD() != null) {
			/* TODO: "this" is a constant ? */
			ret.addOpCode(OpCodes.OpCode.PUSH_THIS);
		}
		else if (context.IDENTIFIER() != null) {
            string identifier = context.IDENTIFIER().GetText();
            if (this.catchExceptionNameStack.Count > 0 && this.catchExceptionNameStack.Peek() == identifier) {
                ret.addOpCode(OpCodes.OpCode.PUSH_EXCEPTION);
            }
            else {
			    ret.addOpCode(OpCodes.OpCode.PUSH_IDENTIFIER);
			    ret.addString(context.IDENTIFIER().GetText());
                /* We have an identifier needing dereferencing ... */
			    ret.constant = false;
            }
		}
		else if (context.OPEN_PARANTHESIS() != null) {
			return this.Visit(context.expression());
		}
		else {
			this.logFatal(context, "Internal Error: Not implemented");
			throw new System.NotImplementedException();
		}

		return ret;
	}

	public override JSCompilerResult VisitObjectLiteral(JSGrammarParser.ObjectLiteralContext context) {
		JSCompilerResult ret = new JSCompilerResult();
		JSGrammarParser.ObjectPropertyNameContext[] properties = context.objectPropertyName();
		JSGrammarParser.ExpressionContext[] expressions = context.expression();

		if (properties.Length != expressions.Length) {
			this.logFatal(context, "Internal Error: Object construction: invalid number of properties and values");
			return null;
		}

		for (int i = 0; i < properties.Length; i++) {
			JSCompilerResult r1 = this.Visit(properties[i]);
			JSCompilerResult r2 = this.Visit(expressions[i]);

			ret.append(r1);
			ret.append(r2);
			if (r2.isConstant() != true) {
				/* Object construction needs raw value */
				ret.addOpCode(OpCodes.OpCode.DEREF);
			}
		}

		ret.constant = true;
		ret.addOpCode(OpCodes.OpCode.PUSH_OBJECT);
		ret.addInt32(properties.Length);

		return ret;
	}

	public override JSCompilerResult VisitObjectPropertyName(JSGrammarParser.ObjectPropertyNameContext context) {
		JSCompilerResult ret = new JSCompilerResult();

		if (context.IDENTIFIER() != null) {
			ret.addOpCode(OpCodes.OpCode.PUSH_STRING);
			ret.addString(context.IDENTIFIER().GetText());
		}
		else if (context.NUMERIC_LITERAL() != null) {
			ret.addOpCode(OpCodes.OpCode.PUSH_NUMBER);
			ret.addFloat(this.tokenToNumber(context.NUMERIC_LITERAL().GetText()));
		}
		else if (context.STRING_LITERAL() != null) {
			ret.addOpCode(OpCodes.OpCode.PUSH_STRING);
			ret.addString(this.tokenToString(context.STRING_LITERAL().GetText()));
		}

		return ret;
	}

    public override JSCompilerResult VisitFunctionFormalParameters(JSGrammarParser.FunctionFormalParametersContext context) {
        JSCompilerResult ret = new JSCompilerResult();

        foreach (var identifier in context.IDENTIFIER()) {
            ret.addLocalVariable(identifier.GetText());
        }

        return ret;
    }

    public override JSCompilerResult VisitFunctionDeclarationHoisted(JSGrammarParser.FunctionDeclarationHoistedContext context) {
        JSCompilerResult ret = new JSCompilerResult();
        JSCompilerResult hoisted_function = new JSCompilerResult();
        JSCompilerResult function = null;
        string name;

        name = context.function_name.Text;

        function = this.buildFunction(name, context.functionFormalParameters(), context.statements(), true);

        /* Build hoisted function */
        hoisted_function.addOpCode(OpCodes.OpCode.PUSH_IDENTIFIER);
        hoisted_function.addString(name);

        hoisted_function.append(function);

        hoisted_function.addOpCode(OpCodes.OpCode.ASSIGN);

        /* Add hoisted function */
        ret.local_variables.Add(name);
        ret.hoisted_functions.Add(hoisted_function);

        return ret;
    }

    public override JSCompilerResult VisitFunctionDeclarationInExpression(JSGrammarParser.FunctionDeclarationInExpressionContext context) {
        JSCompilerResult ret = null;
        string function_name = null;

        if (context.function_name != null) {
            function_name = context.function_name.Text;
        }
        else {
            function_name = "";
        }

        ret = this.buildFunction(function_name, context.functionFormalParameters(), context.statements(), false);

        return ret;
    }
}

}
