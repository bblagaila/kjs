﻿//
//  Program.cs
//
//  Author:
//       Bogdan <${AuthorEmail}>
//
//  Copyright (c) 2015 Bogdan
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//

using System;
using System.Collections;
using System.Collections.Generic;
using Antlr4.Runtime;
using Antlr4.Runtime.Misc;
using Antlr4.Runtime.Atn;
using Antlr4.Runtime.Sharpen;

namespace kjs.parser {

public class Program {
	public JSGrammarParser.ProgramContext program;
	public byte[] code;

	protected Program(JSGrammarParser.ProgramContext program, byte[] code) {
		this.program = program;
		this.code = code;
	}

	public class CompilationErrorLogger : BaseErrorListener {
		public List<string> error_list = new List<string>();

		public override void SyntaxError(IRecognizer recognizer, IToken offendingSymbol, int line, int charPositionInLine,
											string msg, RecognitionException e) {
			String error_message = "Syntax error: line " + line + ":" + charPositionInLine + " " + msg;
			this.error_list.Add(error_message);
		}
	}

	public class CompilationException : Exception {
		public string message;

		public CompilationException(List<string> error_list) {
			this.message = "Compilation error.";
			for (int i = 0; i < error_list.Count; i++) {
				this.message += "\n" + error_list[i];
			}
			this.message = "\n";
		}

		public string getMessage() {
			return this.message;
		}
	}

	public class ParserErrorStrategy : DefaultErrorStrategy {
		public CommonTokenStream token_stream = null;

		public ParserErrorStrategy(CommonTokenStream token_stream) {
			this.token_stream = token_stream;

		}

		protected bool ShouldInsertAutoSemicolon(Parser recognizer, IToken offendingToken) {
			RuleContext context = recognizer.Context;
			while (context != null && context.RuleIndex != JSGrammarParser.RULE_statement) {
				context = context.Parent;
			}

			if (context != null && context.RuleIndex == JSGrammarParser.RULE_statement) {
				if (offendingToken.Type == JSGrammarLexer.CLOSE_CURLY_BRACKET) {
					return true;
				}
				else {
					IList<IToken> lt = this.token_stream.GetHiddenTokensToLeft(offendingToken.TokenIndex, 2);
					if (lt != null && lt.Count > 0) {
						return true;
					}
				}
			}

			return false;
		}

		protected IToken ConstructSemiColon(ITokenSource token_source, IToken current) {
			ITokenFactory token_factory = token_source.TokenFactory;
			IToken ret = token_factory.Create(JSGrammarLexer.SEMICOLON, "<missing ;>");
			return ret;
		}

		public override IToken RecoverInline(Parser recognizer) {
			if (this.ShouldInsertAutoSemicolon(recognizer, recognizer.CurrentToken)) {
				return ConstructSemiColon(recognizer.TokenStream.TokenSource, recognizer.CurrentToken);
			}
			return base.RecoverInline(recognizer);
		}
			
		protected override void ReportInputMismatch(Parser recognizer, InputMismatchException e) {
			if (this.ShouldInsertAutoSemicolon(recognizer, e.OffendingToken) == false) {
				base.ReportInputMismatch(recognizer, e);
			}
		}
			
		protected override void ReportUnwantedToken(Parser recognizer) {
			if (this.ShouldInsertAutoSemicolon(recognizer, recognizer.CurrentToken) == false) {
				base.ReportUnwantedToken(recognizer);
			}
		}
	}

	public static Program compileFromStream(System.IO.Stream source) {
		AntlrInputStream istream = new AntlrInputStream(source);

		// CompilationErrorLogger error_listener = new CompilationErrorLogger();
		JSGrammarLexer lexer = new JSGrammarLexer(istream);
		CommonTokenStream token_stream = null;
		// lexer.RemoveErrorListeners();
		// lexer.AddErrorListener(error_listener);
		// if (error_listener.error_list.Count > 0) {
			// throw new CompilationException(error_listener.error_list);
		// }

		// TODO: DEBUG
		if (true) {
			token_stream = new CommonTokenStream(lexer);
			token_stream.Fill();
			IList<IToken> tokens = token_stream.GetTokens();
			Console.WriteLine("--- TOKENS --- " + tokens.Count + "\n");
			int i = 0;
			foreach (IToken token in tokens) {
				if (token.Channel == 2) {
					continue;
				}
				String token_name = JSGrammarLexer.DefaultVocabulary.GetSymbolicName(token.Type);
				if (token_name == null || token_name.Length < 1) {
					token_name = "'" + token.Text + "'";
				}
				else {
					token_name = token_name + "(" + token.Text + ")";
				}
				Console.Write("" + token_name + " ");
			}
			Console.WriteLine("---\n");

			istream.Reset();
			lexer.Reset();
		}

		token_stream = new CommonTokenStream(lexer);
		CompilationErrorLogger error_listener = new CompilationErrorLogger();
		JSGrammarParser parser = new JSGrammarParser(token_stream);
		// parser.RemoveErrorListeners();
		parser.AddErrorListener(error_listener);
		parser.ErrorHandler = new ParserErrorStrategy(token_stream);

		JSGrammarParser.ProgramContext program_ast = parser.program();
		if (error_listener.error_list.Count > 0) {
			throw new CompilationException(error_listener.error_list);
		}

		// ByteCodeCompilerGrammarVisitor compiler = new ByteCodeCompilerGrammarVisitor();
		JSCompiler compiler = new JSCompiler();
		byte[] code = null;
		code = compiler.compile(program_ast);
		try {
			// code = compiler.compile(program_ast);
		}
		catch (Exception e) {
			throw e;
			Console.WriteLine("Generic compilation error...");
			throw new Program.CompilationException(error_listener.error_list);
		}

		return new Program(program_ast, code);
	}
}

}
