﻿//
//  JSValueOperations.cs
//
//  Author:
//       bogdanb <${AuthorEmail}>
//
//  Copyright (c) 2015 bogdanb
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  aint with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
using System;

using kjs.parser.Value;

namespace kjs.parser {

public class JSValueOperations {
	public static JSValue valuesAdd(JSValue v1, JSValue v2) {
		JSValue ret = null;

		if (v1.type == JSValue.Type.STRING || v2.type == JSValue.Type.STRING) {
			ret = new JSValueString(v1.toString() + v2.toString());
		}
		else {
			JSValueNumber a = new JSValueNumber(v1);
			JSValueNumber b = new JSValueNumber(v2);

			ret = new JSValueNumber(a.numberValue + b.numberValue);
		}

		return ret;
	}

	public static JSValue valuesSub(JSValue v1, JSValue v2) {
		JSValue ret = null;

		JSValueNumber a = new JSValueNumber(v1);
		JSValueNumber b = new JSValueNumber(v2);

		ret = new JSValueNumber(a.numberValue - b.numberValue);

		return ret;
	}

	public static JSValue valuesMult(JSValue v1, JSValue v2) {
		JSValue ret = null;

		JSValueNumber a = new JSValueNumber(v1);
		JSValueNumber b = new JSValueNumber(v2);

		ret = new JSValueNumber(a.numberValue * b.numberValue);

		return ret;
	}

	public static JSValue valuesDiv(JSValue v1, JSValue v2) {
		JSValue ret = null;

		JSValueNumber a = new JSValueNumber(v1);
		JSValueNumber b = new JSValueNumber(v2);

		ret = new JSValueNumber(a.numberValue / b.numberValue);

		return ret;
	}

	public static JSValue valuesRem(JSValue v1, JSValue v2) {
		JSValue ret = null;

		JSValueNumber a = new JSValueNumber(v1);
		JSValueNumber b = new JSValueNumber(v2);

		ret = new JSValueNumber(a.numberValue % b.numberValue);

		return ret;
	}

	public static JSValue valueLShift(JSValue value, JSValue amount) {
		JSValue ret = null;

		JSValueNumber v = new JSValueNumber(value);
		JSValueNumber a = new JSValueNumber(amount);

		if (a.isNaN()) {
			return v;
		}

		ret = new JSValueNumber(((int)(v.getValue())) << ((int)(a.getValue())));

		return ret;
	}

	public static JSValue valueRShift(JSValue value, JSValue amount) {
		JSValue ret = null;

		JSValueNumber v = new JSValueNumber(value);
		JSValueNumber a = new JSValueNumber(amount);

		if (a.isNaN()) {
			return v;
		}

		ret = new JSValueNumber(((int)(v.getValue())) >> ((int)(a.getValue())));

		return ret;
	}

	public static JSValue valueZFRShift(JSValue value, JSValue amount) {
		JSValue ret = null;

		JSValueNumber v = new JSValueNumber(value);
		JSValueNumber a = new JSValueNumber(amount);

		if (a.isNaN()) {
			return v;
		}

		// TODO: What the hell is ZFRShift, and why is it different from RShift,
		// and how to do it in C#
		ret = new JSValueNumber(((int)(v.getValue())) >> ((int)(a.getValue())));

		return ret;
	}

	public static float valuesCompare(JSValue v1, JSValue v2) {
		if (v1.type == JSValue.Type.STRING && v2.type == JSValue.Type.STRING) {
			string a = ((JSValueString)v1).getValue();
			string b = ((JSValueString)v2).getValue();

			return string.Compare(a, b);
		}
		else if (v1.type == JSValue.Type.NUMBER && v2.type == JSValue.Type.NUMBER) {
			JSValueNumber a = new JSValueNumber(v1);
			JSValueNumber b = new JSValueNumber(v2);

			return a.getValue() - b.getValue();
		}
		else {
			// TODO: implement this for other objects ...
		}

		return float.NaN;
	}

	public static float valuesStrictCompare(JSValue v1, JSValue v2) {
		if (v1.type == JSValue.Type.STRING && v2.type == JSValue.Type.STRING) {
			string a = ((JSValueString)v1).getValue();
			string b = ((JSValueString)v2).getValue();

			return string.Compare(a, b);
		}
		else if (v1.type == JSValue.Type.NUMBER && v2.type == JSValue.Type.NUMBER) {
			JSValueNumber a = new JSValueNumber(v1);
			JSValueNumber b = new JSValueNumber(v2);

			return a.getValue() - b.getValue();
		}
		else {
			// TODO: implement this for other objects ...
		}

		return float.NaN;
	}

	public static JSValue valuesBitwiseAnd(JSValue v1, JSValue v2) {
		if (v1.type == JSValue.Type.NUMBER && v2.type == JSValue.Type.NUMBER) {
			JSValueNumber a = new JSValueNumber(v1);
			JSValueNumber b = new JSValueNumber(v2);

			return new JSValueNumber(((int)a.getValue()) & ((int)b.getValue()));
		}
		return new JSValueNumber(0.0f);
	}

	public static JSValue valuesBitwiseOr(JSValue v1, JSValue v2) {
		if (v1.type == JSValue.Type.NUMBER && v2.type == JSValue.Type.NUMBER) {
			JSValueNumber a = new JSValueNumber(v1);
			JSValueNumber b = new JSValueNumber(v2);

			return new JSValueNumber(((int)a.getValue()) | ((int)b.getValue()));
		}
		else if (v1.type == JSValue.Type.NUMBER) {
			return v1;
		}
		else if (v2.type == JSValue.Type.NUMBER) {
			return v2;
		}
		return new JSValueNumber(0.0f);
	}

	public static JSValue valuesBitwiseXor(JSValue v1, JSValue v2) {
		if (v1.type == JSValue.Type.NUMBER && v2.type == JSValue.Type.NUMBER) {
			JSValueNumber a = new JSValueNumber(v1);
			JSValueNumber b = new JSValueNumber(v2);

			return new JSValueNumber(((int)a.getValue()) ^ ((int)b.getValue()));
		}
		else if (v1.type == JSValue.Type.NUMBER) {
			return v1;
		}
		else if (v2.type == JSValue.Type.NUMBER) {
			return v2;
		}
		return new JSValueNumber(0.0f);
	}

	public static JSValue valuesLogicalAnd(JSValue v1, JSValue v2) {
		JSValueBoolean a = new JSValueBoolean(v1);
		JSValueBoolean b = new JSValueBoolean(v2);

		if (a.getValue() == false) {
			return a;
		}
		return b;
	}

	public static JSValue valuesLogicalOr(JSValue v1, JSValue v2) {
		JSValueBoolean a = new JSValueBoolean(v1);
		JSValueBoolean b = new JSValueBoolean(v2);

		if (a.getValue() == true) {
			return a;
		}
		return b;
	}
}

}
