﻿/*
 *  OpCodes.cs
 *
 *  Author:
 *       Bogdan Blagaila <blbogdan2000@gmail.com>
 *
 *  Copyright (c) 2014 Bogdan Blagaila
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

using System;

namespace kjs.parser {

public class OpCodes {
	public enum OpCode {
		EMPTY = 0, /* Note: this is not NOP, this is an invalid instruction */

		POP, /* Pops the last value from the stack and discards it */

		PUSH_NULL,
		PUSH_UNDEFINED,
		PUSH_BOOLEAN_TRUE,
		PUSH_BOOLEAN_FALSE,
		PUSH_NUMBER, /* 4/8 byte: float */
		PUSH_STRING, /* x bytes: string, null terminated */
		PUSH_REGEX, /* x bytes: string, null terminated */
		PUSH_ARRAY, /* 4/8 byte: size_t ; each value must be on stack */
		PUSH_OBJECT, /* 4/8 byte: size_t ; property name + value pairs must be on stack */
		PUSH_FUNCTION, /* size_t (jump over offset),
							string name, // empty is anonymous
							size_t (nr_parameters), <parameters_array>,
							size_t (nr_variables), <local_variables_names_array>,
							the code,
							RET statement
							*/
        PUSH_FUNCTION_HOISTED, /* size_t (jump over offset),
							string name, // empty is anonymous
							size_t (nr_parameters), <parameters_array>,
							size_t (nr_variables), <local_variables_names_array>,
							the code,
							RET statement
							*/
		PUSH_IDENTIFIER, /* x bytes: string, null terminated */
		PUSH_THIS, /* 0 bytes, pushes this argument */

        PUSH_EXCEPTION, /* 0 bytes, used inside catch(...) { .. } block. Pushes the caught exception on stack */

		DEREF, /* 0 bytes; dereferences current reference on stack; replaces it with it's value ;
				* calls accessor if needed ; if value not a reference, it remains unchanged */

		DEREF_DUP, /* 0 bytes; same as deref, but pushes the original reference on top of the stack */

		DUP,	/* 0 bytes; duplicate last variable from stack */
		DUPN, /* 1 bytes offset ; copy variable from stack (0 - top, -1 - previous, etc ...) */

		CALL, /* 4/8 byte: size_t (number of arguments); requires stack structure: [function], (arguments)* ; all dereferenced */
		MEMBER_ID, /* req stack structure: [object], [identifier] ; combines reference [object.id] */
		MEMBER, /* req stack structure: [object], [value] ; combines reference [object.value_as_string] */
		INC, /* increment last stack value; must be reference; dereferences value; keeps new value */
		DEC, /* decrement last stack value; must be reference; dereferences value; keeps new value */
		MINUS, /* negates current stack value */
		PLUS, /* positives current stack value (does nothing for number; converts to number; dereferences value) */
		B_NOT, /* bit-wise negates the current stack value */
		L_NOT, /* logical negates the current stack value */
		DELETE, /* delete the current reference (from current stack value) */
		TYPEOF, /* returns the type of the current stack value */

		MULT, /* multiplicates last two stack values; returns one value */
		DIV, /* divides last two stack values; returns one value */
		REM, /* divides last two stack values returns remainder; returns one value */

		ADD, /* adds last two stack values; returns one value */
		SUB, /* substracts last two stack values; returns one value */

		LSHIFT, /* shifts last stack value left */
		RSHIFT, /* shifts last stack value right */
		ZFRSHIFT, /* shifts last stack value right to unsigned value */

		IS_LT, /* stack: [v1], [v2]. Replaces them with a boolean true if v1 < v2 */
		IS_LTE, /* same but <= */
		IS_GT, /* > */
		IS_GTE, /* >= */
		IS_EQ, /* == */
		IS_NEQ, /* !== */
		IS_SEQ, /* === */
		IS_NSEQ, /* !== */

		INSTANCEOF, /* stack: [v1], [v2]. Is v1 instance of v2 ? */
		IN, /* stack: [v1], [v2]. Is v1 contained in v2 ? */

		B_AND, /* stack: [v1], [v2]. result: v1 & v2 */
		B_XOR, /* stack: [v1], [v2]. result: v1 ^ v2 */
		B_OR, /* stack: [v1], [v2]. result: v1 | v2 */

		L_AND, /* stack: [v1], [v2]. result: v1 && v2 */
		L_OR, /* stack: [v1], [v2]. result: v1 || v2 */

		ASSIGN, /* stack structure: [reference], [value]; assign value to reference */

		RET, /* returns from function call; return value (current stack value) added on stack of caller (push'd last) */

		JMP, /* 4/8 bytes: size_t (jump offset) ; jumps without condition */
		JF, /* 4/8 bytes: size_t (jump offset) ; jumps if current value on stack evaluates to false */
        JT, /* 4/8 bytes: size_t (jump offset) ; jumps if current value on stack evaluates to true */

		NEW, /* 4/8 byte: size_t (number of arguments); requires stack structure: [function], (arguments)* */

        THROW, /* stack: [v1]; throws exception with v1 as exception object */
        TRY_BEGIN, /* size_t (jump offset); jumps to the specified offset in the
                    * current call frame if an exception is thrown before TRY_END */
        TRY_END, /* Marks the end of the current exception catching context */

		NOP, /* Does nothing */

		_LAST
	};

	public static string[] names = new string[(int)OpCode._LAST];

	public OpCodes() {
		names[(int)OpCode.EMPTY] = "<EMPTY>";
		names[(int)OpCode.PUSH_NULL] = "PUSH_NULL";
		names[(int)OpCode.PUSH_UNDEFINED] = "PUSH_UNDEFINED";
		names[(int)OpCode.PUSH_BOOLEAN_TRUE] = "PUSH_BOOLEAN_TRUE";
		names[(int)OpCode.PUSH_BOOLEAN_FALSE] = "PUSH_BOOLEAN_FALSE";
		names[(int)OpCode.PUSH_NUMBER] = "PUSH_NUMBER";
		names[(int)OpCode.PUSH_STRING] = "PUSH_STRING";
		names[(int)OpCode.PUSH_REGEX] = "PUSH_REGEX";
		names[(int)OpCode.PUSH_ARRAY] = "PUSH_ARRAY";
		names[(int)OpCode.PUSH_OBJECT] = "PUSH_OBJECT";
		names[(int)OpCode.PUSH_FUNCTION] = "PUSH_FUNCTION";
        names[(int)OpCode.PUSH_FUNCTION_HOISTED] = "PUSH_FUNCTION_HOISTED";
		names[(int)OpCode.PUSH_IDENTIFIER] = "PUSH_IDENTIFIER";
		names[(int)OpCode.PUSH_THIS] = "PUSH_THIS";
        names[(int)OpCode.PUSH_EXCEPTION] = "PUSH_EXCEPTION";
		names[(int)OpCode.POP] = "POP";
		names[(int)OpCode.DEREF] = "DEREF";
		names[(int)OpCode.DEREF_DUP] = "DEREF_DUP";
		names[(int)OpCode.DUP] = "DUP";
		names[(int)OpCode.DUPN] = "DUPN";
		names[(int)OpCode.CALL] = "CALL";
		names[(int)OpCode.MEMBER_ID] = "MEMBER_ID";
		names[(int)OpCode.MEMBER] = "MEMBER";
		names[(int)OpCode.INC] = "INC";
		names[(int)OpCode.DEC] = "DEC";
		names[(int)OpCode.MINUS] = "MINUS";
		names[(int)OpCode.PLUS] = "PLUS";
		names[(int)OpCode.B_NOT] = "B_NOT";
		names[(int)OpCode.L_NOT] = "L_NOT";
		names[(int)OpCode.DELETE] = "DELETE";
		names[(int)OpCode.TYPEOF] = "TYPEOF";
		names[(int)OpCode.MULT] = "MULT";
		names[(int)OpCode.DIV] = "DIV";
		names[(int)OpCode.REM] = "REM";
		names[(int)OpCode.ADD] = "ADD";
		names[(int)OpCode.SUB] = "SUB";
		names[(int)OpCode.LSHIFT] = "LSHIFT";
		names[(int)OpCode.RSHIFT] = "RSHIFT";
		names[(int)OpCode.ZFRSHIFT] = "ZFRSHIFT";
		names[(int)OpCode.IS_LT] = "LT";
		names[(int)OpCode.IS_LTE] = "LTE";
        names[(int)OpCode.IN] = "IN";
        names[(int)OpCode.INSTANCEOF] = "INSTANCEOF";
		names[(int)OpCode.IS_EQ] = "EQ";
		names[(int)OpCode.IS_NEQ] = "NEQ";
		names[(int)OpCode.IS_SEQ] = "SEQ";
		names[(int)OpCode.IS_NSEQ] = "NSEQ";
		names[(int)OpCode.IS_GT] = "GT";
		names[(int)OpCode.IS_GTE] = "GTE";
		names[(int)OpCode.B_AND] = "B_AND";
		names[(int)OpCode.B_OR] = "B_OR";
		names[(int)OpCode.B_XOR] = "B_XOR";
		names[(int)OpCode.L_AND] = "L_AND";
		names[(int)OpCode.L_OR] = "L_OR";
		names[(int)OpCode.ASSIGN] = "ASSIGN";
		names[(int)OpCode.RET] = "RET";
		names[(int)OpCode.JF] = "JF";
        names[(int)OpCode.JT] = "JT";
		names[(int)OpCode.JMP] = "JMP";
		names[(int)OpCode.NEW] = "NEW";
        names[(int)OpCode.THROW] = "THROW";
        names[(int)OpCode.TRY_BEGIN] = "TRY_BEGIN";
        names[(int)OpCode.TRY_END] = "TRY_END";
		names[(int)OpCode.NOP] = "NOP";
	}

	public static string readString(byte[] code, ref int pos) {
		int k, len = 0;
		string ret = "";

		for (k = pos; k < code.Length; k++) {
			if (code[k] == 0) {
				break;
			}
		}
		len = k - pos;
		if (len > 0) {
			ret = new string(System.Text.Encoding.UTF8.GetChars(code, (int)pos, (int)(k - pos)));
		}
		pos = k + 1;
		return ret;
	}

	public static float readFloat(byte[] code, ref int pos) {
		float f = BitConverter.ToSingle(code, (int)pos);
		pos += sizeof(float);
		return f;
	}

	public static int readinteger(byte[] code, ref int pos) {
		int f = BitConverter.ToInt32(code, pos);
		pos += sizeof(float);
		return f;
	}

	public static string dumpCode(byte[] code) {
		string ret = "";

		for (int i = 0; i < code.Length; ) {
			OpCode op_code = (OpCode)code[i];

			ret += "" + i + ": ";
			// ret += String.Format("{0:d04}: ", (int)op_code);
			ret += names[(int)op_code];

			i++;

			switch (op_code) {
				case OpCode.PUSH_NUMBER:
					ret += " " + readFloat(code, ref i);
					break;
				
				case OpCode.PUSH_OBJECT:
				case OpCode.DUPN:
				case OpCode.JMP:
				case OpCode.JF:
                case OpCode.JT:
				case OpCode.CALL:
				case OpCode.NEW:
                case OpCode.TRY_BEGIN:
					ret += " " + readinteger(code, ref i);
					break;

				case OpCode.PUSH_STRING:
				case OpCode.PUSH_IDENTIFIER:
					ret += " " + readString(code, ref i);
					break;

				case OpCode.PUSH_FUNCTION:
                case OpCode.PUSH_FUNCTION_HOISTED:
					{
						int fn_len = readinteger(code, ref i);

						ret += " " + fn_len;

						ret += " " + readString(code, ref i);

						ret += "(";
						int fn_args_len = readinteger(code, ref i);
						for (int k = 0; k < fn_args_len; k++) {
							ret += readString(code, ref i) + ",";
						}
						ret += ") ";

						ret += " vars: ";
						int fn_vars_len = readinteger(code, ref i);
						for (int k = 0; k < fn_vars_len; k++) {
							ret += readString(code, ref i) + ",";
						}
					}
					break;
				
				default:
					break;
			}

			// Console.WriteLine(ret); ret = "";
			ret += "\n";
		}

		return ret;
	}
}

}
