﻿/*
 *  JSVM.cs
 *
 *  Author:
 *       Bogdan Blagaila <blbogdan2000@gmail.com>
 *
 *  Copyright (c) 2014 Bogdan Blagaila
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

using System;
using System.Collections;
using System.Collections.Generic;

using kjs.parser.Value;

namespace kjs.parser {

public class JSVM {
    public class JStack<T> {
        protected List<T> _stack = new List<T>();

        public T peek() {
            return this._stack[0];
        }

        public T pop() {
            T ret = this._stack[0];
            this._stack.RemoveAt(0);
            return ret;
        }

        public void push(T value) {
            this._stack.Insert(0, value);
        }

        public T at(int index) {
            return this._stack[index];
        }

        public void set(int index, T value) {
            this._stack[index] = value;
        }

        public int size() {
            return this._stack.Count;
        }
    };

    public class ExceptionHandlerContext {
        public int absolute_ip;
        public JSValue caught_exception;
    };

	public class CallFrame {
		public byte[] code = null;
		public int ip = 0;
		public int last_ip = 0;

		public int prev_eval_stack_size = 0;

		public JSValueObject scope = null;
		public JSValueObject this_object = null;
		public JSValueFunction function = null;
		public bool is_constructor = false;
        public bool is_setter = false;
        public bool is_getter = false;
        public int getter_stack_offset = -1;

        public Stack<ExceptionHandlerContext> exception_handlers = new Stack<ExceptionHandlerContext>();
	};


	/* VM State */
	public JSValueObject global_scope = new JSValueObject();

    public JStack<CallFrame> call_stack = new JStack<CallFrame>();

    public JStack<JSValue> eval_stack = new JStack<JSValue>();


	/* Constructor */
	public JSVM() {
	}

	/**
	 * Start interpreting the given program
	 */
	public void eval(JSCode program) {
		CallFrame frame = new CallFrame();

		frame.code = program.code;
		frame.ip = 0;
		frame.scope = this.global_scope;
		frame.this_object = this.global_scope;

        this.call_stack.push(frame);
	}

	/**
	 * Execute all opcodes in one run, until the call stack is empty
	 */
	public void executeAll() {
		CallFrame frame;

		while (this.call_stack.size() > 0) {
			frame = this.call_stack.peek();
			if (frame == null) {
				break;
			}
            else if (frame.ip >= frame.code.Length) {
                this.call_stack.pop();
                break;
            }

            try {
			    this.executeOpCode();
            }
            catch (Exception e) {
                Console.WriteLine("Exception at opcode " + frame.ip);
                throw e;
            }

            /* FIXME: VM Debugging */
            if (false) {
                string msg = "";

                msg += "IP(" + frame.ip + ") ";
                msg += "Stack(";
                for (int i = 0; i < this.eval_stack.size(); i++) {
                    msg += this.eval_stack.at(i).toString() + "; ";
                }
                msg += ")";

                Console.WriteLine(msg);
            }
		}
	}

	/**
	 * Read next opcode
	 */
	protected OpCodes.OpCode readOpCode(CallFrame frame) {
		frame.last_ip = frame.ip;
		return (OpCodes.OpCode)frame.code[frame.ip++];
	}

	/**
	 * Read next 32-bit integer
	 */
	protected int readInteger(CallFrame frame) {
		int number;

		number = BitConverter.ToInt32(frame.code, frame.ip);
		frame.ip += sizeof(Int32);

		return number;
	}

	/**
	 * Read next float
	 */
	protected float readFloat(CallFrame frame) {
		float number;

		number = BitConverter.ToSingle(frame.code, frame.ip);
		frame.ip += sizeof(float);

		return number;
	}

	/**
	 * Read next string
	 */
	protected string readString(CallFrame frame) {
		string ret;
		int start_position;

		start_position = frame.ip;
		for (; frame.ip < frame.code.Length; frame.ip++) {
			if (frame.code[frame.ip] == 0) {
				break;
			}
		}

		ret = new string(System.Text.Encoding.UTF8.GetChars(frame.code, start_position, (frame.ip - start_position)));
		frame.ip += 1;

		return ret;
	}

    /**
     * Pop a value from the eval stack. Assert the value is not reference.
     */
    public JSValue popValueEvalStack() {
        JSValue ret = null;

        ret = this.eval_stack.pop();

        if (ret.type == JSValue.Type.REFERENCE) {
            // TODO: Throw a runtime error
        }

        return ret;
    }

	/**
	 * Make a JS function call. Add the function to the call stack.
	 */
	public void callFunction(JSValueFunction function, JSValueObject _this, JSValue[] arguments,
                                bool is_constructor = false, bool is_setter = false, bool is_getter = false,
                                int getter_stack_offset = -1) {
		CallFrame frame;

		if (function.isNative()) {
			/* Call native function; blocking until returns */
			JSValue ret = function.native_function(_this, arguments);
			if (ret == null) {
				this.eval_stack.push(JSValue.Undefined());
			}
			else {
				this.eval_stack.push(ret);
			}

			return;
		}

		/* Create new call frame */
		frame = new CallFrame();
		frame.code = function.code_ref;
		frame.ip = function.code_start_offset;
		frame.last_ip = frame.ip;
		frame.prev_eval_stack_size = this.eval_stack.size();
		frame.scope = new JSValueObject();
		frame.this_object = _this;
		frame.function = function;
		frame.is_constructor = is_constructor;
        frame.is_setter = is_setter;
        frame.is_getter = is_getter;
        frame.getter_stack_offset = getter_stack_offset;

		/* Add local variables */
		for (int i = 0; i < function.local_variables.Length; i++) {
			frame.scope.set(function.local_variables[i], JSValue.Undefined());
		}

		/* Add call parameters */
		for (int i = 0; i < function.formal_parameters.Length; i++) {
			if (i < arguments.Length) {
				frame.scope.set(function.formal_parameters[i], arguments[i]);
			}
			else {
				frame.scope.set(function.formal_parameters[i], JSValue.Undefined());
			}
		}

		/* Push frame to call stack */
		this.call_stack.push(frame);
	}

	/**
	 * Handles the return from a function call
	 */
	public void returnFromFunction(JSValue returnValue) {
		CallFrame prev_frame;

		/* Pop call frame */
		prev_frame = this.call_stack.peek();
		this.call_stack.pop();

		while (this.eval_stack.size() > prev_frame.prev_eval_stack_size) {
			this.eval_stack.pop();
		}

        if (prev_frame.is_setter == true) {
            /* Don't return anything */
        }
        else if (prev_frame.is_getter == true) {
            if (prev_frame.getter_stack_offset == -1) {
                this.eval_stack.push(returnValue);
            }
            else {
                this.eval_stack.set(prev_frame.getter_stack_offset, returnValue);
            }
        }
		else if (prev_frame.is_constructor && returnValue.type != JSValue.Type.OBJECT) {
			this.eval_stack.push(prev_frame.this_object);
		}
		else {
			this.eval_stack.push(returnValue);
		}
	}

    /**
     * Throws an exception
     */
    public void throwException(JSValue exception) {
        while (this.call_stack.size() > 0) {
            CallFrame frame = this.call_stack.peek();

            while (frame.exception_handlers.Count > 0) {
                ExceptionHandlerContext handler = frame.exception_handlers.Peek();

                if (handler.caught_exception == null) {
                    handler.caught_exception = exception;
                    frame.ip = handler.absolute_ip;
                    return;
                }

                frame.exception_handlers.Pop();
            }

            this.call_stack.pop();
        }

        /* Unhandled exception */
        // TODO: a way to report it to the client code
        Console.WriteLine("Unhandled Exception: " + exception.toString());
    }

    /**
     * Returns the parent object containing the variable/property referred by the reference
     */
    public JSValueObject getReferenceParent(JSValueReference reference) {
        CallFrame frame;
        string name;

        if (reference.refObject != null) {
            return reference.refObject;
        }

        name = reference.refProperty;

        frame = this.call_stack.peek();

        if (frame != null) {
            /* 1, Try the local scope */
            if (frame.scope.hasProperty(name)) {
                return frame.scope;
            }
            
            /* 2. Try the closures */
            if (frame.function != null) {
                JSValueObject[] closures = frame.function.closure;
                for (int i = closures.Length - 1; i >= 0; i--) {
                    if (closures[i].hasProperty(name)) {
                        return closures[i];
                    }
                }
            }
        }

        /* 3. Finally, the global scope */
        return this.global_scope;
    }

    /**
     * Retrieves the JSValue from the property referred by the given reference
     */
    public void referenceGet(JSValueReference reference, int stack_index = -1) {
        JSValueObject parent;
        JSValueObject.Property property;
        bool throws_ref_error;
        JSValue returned_value = null;

        throws_ref_error = (reference.refObject == null) ? true : false;

        parent = this.getReferenceParent(reference);
        property = parent.get(reference.refProperty);

        if (property == null) {
            if (throws_ref_error) {
                /* Throw run-time error */
                this.throwRuntimeError("ReferenceError: " + reference.refProperty + " is not defined");
                returned_value = JSValue.Undefined();
            }
            else {
                /* Return undefined */
                returned_value = JSValue.Undefined();
            }
        }
        else if (property.getType() == JSValueObject.Property.Type.DATA) {
            returned_value = ((JSValueObject.DataProperty)property).value;
        }
        else if (property.getType() == JSValueObject.Property.Type.ACCESSOR) {
            JSValueObject.AccessorProperty accesor = (JSValueObject.AccessorProperty)property;

            /* Make a getter call */
            if (accesor.getter != null) {
                this.callFunction(accesor.getter, parent, null, false, false, true, stack_index);
                return;
            }
            else {
                returned_value = JSValue.Undefined();
            }
        }

        if (stack_index == -1) {
            this.eval_stack.push(returned_value);
        }
        else {
            this.eval_stack.set(stack_index, returned_value);
        }
    }

    /**
     * Assigns the given JSValue to the given Reference
     */
    public void referenceSet(JSValueReference reference, JSValue value) {
        JSValueObject parent = null;

        parent = this.getReferenceParent(reference);

        JSValueObject.Property property = parent.get(reference.refProperty);

        if (property == null) {
            parent.set(reference.refProperty, value);
        }
        else if (property.getType() == JSValueObject.Property.Type.DATA) {
            parent.set(reference.refProperty, value);
        }
        else if (property.getType() == JSValueObject.Property.Type.ACCESSOR) {
            JSValueObject.AccessorProperty accessor = (JSValueObject.AccessorProperty)property;
            
            /* Make a setter call */
            if (accessor.setter != null) {
                JSValue[] arguments = new JSValue[1];
                arguments[0] = value;

                this.callFunction(accessor.setter, parent, arguments, false, true);
            }
        }
    }

    /**
     * Deletes the property specified by the given reference from it's parent
     */
    public bool referenceDelete(JSValueReference reference) {
        // TODO: ...
        return false;
    }

    /**
     * Throws a run-time error
     */
    public void throwRuntimeError(string message) {
        // TODO: Implement run-time exceptions based on Exception native object
        this.throwException(new JSValueString("Runtime Error: " + message));
    }

    /**
	 * Execute one opcode
	 */
	public void executeOpCode() {
		CallFrame frame;
		OpCodes.OpCode opcode;

		frame = this.call_stack.peek();
		opcode = this.readOpCode(frame);

        switch (opcode) {
            case OpCodes.OpCode.POP:
                this.eval_stack.pop();
                break;

            case OpCodes.OpCode.PUSH_UNDEFINED:
                this.eval_stack.push(JSValue.Undefined());
                break;
            
            case OpCodes.OpCode.PUSH_NULL:
                this.eval_stack.push(JSValue.Null());
                break;

            case OpCodes.OpCode.PUSH_BOOLEAN_TRUE:
                this.eval_stack.push(new JSValueBoolean(true));
                break;

            case OpCodes.OpCode.PUSH_BOOLEAN_FALSE:
                this.eval_stack.push(new JSValueBoolean(false));
                break;

            case OpCodes.OpCode.PUSH_NUMBER:
                this.eval_stack.push(new JSValueNumber(this.readFloat(frame)));
                break;

            case OpCodes.OpCode.PUSH_STRING:
                this.eval_stack.push(new JSValueString(this.readString(frame)));
                break;

            case OpCodes.OpCode.PUSH_REGEX:
                /* TODO: Implement ... */
                throw new System.NotImplementedException();
                break;

            case OpCodes.OpCode.PUSH_ARRAY:
                /* TODO: Implement ... */
                throw new System.NotImplementedException();
                break;

            case OpCodes.OpCode.PUSH_OBJECT:
                {
                    /* TODO: Create object with the built-in Object() as prototype */
                    JSValueObject obj = new JSValueObject();
                    int num_items = this.readInteger(frame);

                    for (int i = 0; i < num_items; i++) {
                        JSValue value = this.popValueEvalStack();
                        string name = this.eval_stack.pop().toString();

                        obj.set(name, value);
                    }

                    this.eval_stack.push(obj);
                }
                break;

            case OpCodes.OpCode.PUSH_FUNCTION:
            case OpCodes.OpCode.PUSH_FUNCTION_HOISTED:
                {
                    JSValueFunction function = new JSValueFunction();
                    int function_size = this.readInteger(frame);
                    int function_end_offset = frame.ip + function_size;
                    string function_name = this.readString(frame);

                    /* Function name */
                    function.name = function_name;

                    /* Formal parameters */
                    int nr_parameters = this.readInteger(frame);
                    function.formal_parameters = new string[nr_parameters];
                    for (int i = 0; i < nr_parameters; i++) {
                        function.formal_parameters[i] = this.readString(frame);
                    }

                    /* Local variables */
                    int nr_variables = this.readInteger(frame);
                    function.local_variables = new string[nr_variables];
                    for (int i = 0; i < nr_variables; i++) {
                        function.local_variables[i] = this.readString(frame);
                    }

                    /* Function code */
                    function.code_ref = frame.code;
                    // TODO: debuginfo
                    function.code_start_offset = frame.ip;

                    frame.ip = function_end_offset;

                    /* Create the closure */
                    int nr_closures = 0;

                    if (frame.function != null) {
                        nr_closures = frame.function.closure.Length + 1;
                    }
                    else {
                        nr_closures = 1;
                    }

                    JSValueObject[] closures = new JSValueObject[nr_closures];
                    for (int i = 0; i < nr_closures - 1; i++) {
                        closures[i] = frame.function.closure[i];
                    }
                    closures[nr_closures - 1] = frame.scope;

                    function.closure = closures;

                    /* Push function */
                    this.eval_stack.push(function);

                    /* Set name, if not anonymous */
                    if (opcode == OpCodes.OpCode.PUSH_FUNCTION_HOISTED && function.name.Length > 0) {
                        JSValueReference reference = new JSValueReference(null, function.name);
                        this.referenceSet(reference, function);
                    }
                }
                break;

            case OpCodes.OpCode.PUSH_IDENTIFIER:
                this.eval_stack.push(new JSValueReference(null, this.readString(frame)));
                break;

            case OpCodes.OpCode.PUSH_THIS:
                this.eval_stack.push(frame.this_object);
                break;

            case OpCodes.OpCode.PUSH_EXCEPTION:
                if (frame.exception_handlers.Count < 1) {
                    this.throwRuntimeError("Internal Error: PUSH_EXCEPTION outside of catch() {..} context");
                }
                else {
                    this.eval_stack.push(frame.exception_handlers.Peek().caught_exception);
                }
                break;

            case OpCodes.OpCode.CALL:
            case OpCodes.OpCode.NEW:
                {
                    int num_parameters = this.readInteger(frame);
                    JSValue[] parameters = new JSValue[num_parameters];
                    for (int i = 0; i < num_parameters; i++) {
                        parameters[i] = this.popValueEvalStack();
                    }

                    JSValueObject parent = null;
                    string fn_name = null;
                    JSValue fnCandidate = this.popValueEvalStack();
                    JSValue fnReference = this.eval_stack.pop();

                    if (fnReference.type == JSValue.Type.REFERENCE) {
                        parent = ((JSValueReference)fnReference).refObject;
                        fn_name = ((JSValueReference)fnReference).refProperty;
                    }

                    if (fnCandidate.type != JSValue.Type.FUNCTION) {
                        this.throwRuntimeError("TypeError: " + fnCandidate.getTypeString() + " is not a function");
                        break;
                    }

                    JSValueFunction function = (JSValueFunction)fnCandidate;
                    JSValueObject _this = this.global_scope;
                    if (parent != null) {
                        _this = parent;
                    }

                    if (opcode == OpCodes.OpCode.CALL) {
                        this.callFunction(function, _this, parameters);
                    }
                    else {
                        /* TODO: Create object with the built-in Object() as prototype */
                        _this = new JSValueObject();

                        if (function.name.Length > 0) {
                            _this.name = function.name;
                        }
                        else if (fn_name != null) {
                            _this.name = fn_name;
                        }
                        else {
                            _this.name = "Object";
                        }

                        JSValue prototype = function.getDataValue("prototype");
                        if (prototype.isObject()) {
                            _this.setPrototypeOf(prototype);
                        }

                        this.callFunction(function, _this, parameters, true);
                    }
                }
                break;

            case OpCodes.OpCode.MEMBER_ID:
                {
                    JSValue v1 = this.eval_stack.pop();
                    JSValue parent = this.popValueEvalStack();

                    if (v1.type != JSValue.Type.REFERENCE) {
                        this.throwRuntimeError("Member access with no identifier");
                        break;
                    }

                    JSValueReference property_name = (JSValueReference)v1;

                    // TODO: Convert parent to object if not
                    if (parent.isObject() == false) {
                        this.throwRuntimeError("Primitive2Object not implemented yet");
                        break;
                    }

                    this.eval_stack.push(new JSValueReference((JSValueObject)parent, property_name.refProperty));
                }
                break;

            case OpCodes.OpCode.MEMBER:
                {
                    JSValue property = this.popValueEvalStack();
                    JSValue parent = this.popValueEvalStack();

                    // TODO: Convert parent to object if not
                    if (parent.isObject() == false) {
                        this.throwRuntimeError("Primitive2Object not implemented yet");
                        break;
                    }

                    this.eval_stack.push(new JSValueReference((JSValueObject)parent, property.toString()));
                }
                break;

            case OpCodes.OpCode.DEREF:
            case OpCodes.OpCode.DEREF_DUP:
                {
                    JSValue v1 = this.eval_stack.pop();

                    if (v1.type != JSValue.Type.REFERENCE) {
                        this.eval_stack.push(v1);

                        if (opcode == OpCodes.OpCode.DEREF_DUP) {
                            /* Also, duplicate it */
                            this.eval_stack.push(v1);
                        }
                        break;
                    }

                    JSValueReference reference = (JSValueReference)v1;

                    if (opcode == OpCodes.OpCode.DEREF_DUP) {
                        this.eval_stack.push(v1);
                        this.eval_stack.push(v1);
                        this.referenceGet(reference, 1);
                    }
                    else {
                        this.referenceGet(reference);
                    }
                }
                break;

            case OpCodes.OpCode.DUP:
                this.eval_stack.push(this.eval_stack.peek());
                break;

            case OpCodes.OpCode.DUPN:
                {
                    int offset = this.readInteger(frame);
                    this.eval_stack.push(this.eval_stack.at(offset));
                }
                break;

            case OpCodes.OpCode.INC:
            case OpCodes.OpCode.DEC:
                {
                    JSValueNumber new_value = new JSValueNumber(this.popValueEvalStack());

                    if (opcode == OpCodes.OpCode.INC) {
                        ++new_value.numberValue;
                    }
                    else {
                        --new_value.numberValue;
                    }

                    this.eval_stack.push(new_value);
                }
                break;

            case OpCodes.OpCode.MINUS:
            case OpCodes.OpCode.PLUS:
                {
                    JSValueNumber new_value = new JSValueNumber(this.popValueEvalStack());

                    if (opcode == OpCodes.OpCode.MINUS) {
                        new_value.numberValue = -new_value.numberValue;
                    }

                    this.eval_stack.push(new_value);
                }
                break;

            case OpCodes.OpCode.B_NOT:
                {
                    JSValueNumber new_value = new JSValueNumber(this.popValueEvalStack());

                    new_value.numberValue = (float)(~((int)new_value.numberValue));

                    this.eval_stack.push(new_value);
                }
                break;

            case OpCodes.OpCode.L_NOT:
                {
                    JSValueBoolean new_value = new JSValueBoolean(this.popValueEvalStack());
                    new_value.booleanValue = !new_value.booleanValue;
                    this.eval_stack.push(new_value);
                }
                break;

            case OpCodes.OpCode.DELETE:
                {
                    JSValue v1 = this.eval_stack.pop();

                    if (v1.type == JSValue.Type.REFERENCE) {
                        bool ret = this.referenceDelete((JSValueReference)v1);
                        this.eval_stack.push(new JSValueBoolean(ret));
                    }

                    this.eval_stack.push(JSValueBoolean.True());
                }
                break;

            case OpCodes.OpCode.TYPEOF:
                this.eval_stack.push(new JSValueString(this.popValueEvalStack().getTypeString()));
                break;

            case OpCodes.OpCode.MULT:
                {
                    JSValue v2 = this.popValueEvalStack();
                    JSValue v1 = this.popValueEvalStack();
                    this.eval_stack.push(JSValueOperations.valuesMult(v1, v2));
                }
                break;

            case OpCodes.OpCode.DIV:
                {
                    JSValue v2 = this.popValueEvalStack();
                    JSValue v1 = this.popValueEvalStack();
                    this.eval_stack.push(JSValueOperations.valuesDiv(v1, v2));
                }
                break;

            case OpCodes.OpCode.REM:
                {
                    JSValue v2 = this.popValueEvalStack();
                    JSValue v1 = this.popValueEvalStack();
                    this.eval_stack.push(JSValueOperations.valuesRem(v1, v2));
                }
                break;

            case OpCodes.OpCode.ADD:
                {
                    JSValue v2 = this.popValueEvalStack();
                    JSValue v1 = this.popValueEvalStack();
                    this.eval_stack.push(JSValueOperations.valuesAdd(v1, v2));
                }
                break;

            case OpCodes.OpCode.SUB:
                {
                    JSValue v2 = this.popValueEvalStack();
                    JSValue v1 = this.popValueEvalStack();
                    this.eval_stack.push(JSValueOperations.valuesSub(v1, v2));
                }
                break;

            case OpCodes.OpCode.LSHIFT:
                {
                    JSValue v2 = this.popValueEvalStack();
                    JSValue v1 = this.popValueEvalStack();
                    this.eval_stack.push(JSValueOperations.valueLShift(v1, v2));
                }
                break;

            case OpCodes.OpCode.RSHIFT:
                {
                    JSValue v2 = this.popValueEvalStack();
                    JSValue v1 = this.popValueEvalStack();
                    this.eval_stack.push(JSValueOperations.valueRShift(v1, v2));
                }
                break;

            case OpCodes.OpCode.ZFRSHIFT:
                {
                    JSValue v2 = this.popValueEvalStack();
                    JSValue v1 = this.popValueEvalStack();
                    this.eval_stack.push(JSValueOperations.valueZFRShift(v1, v2));
                }
                break;

            case OpCodes.OpCode.IS_LT:
                {
                    JSValue v2 = this.popValueEvalStack();
                    JSValue v1 = this.popValueEvalStack();
                    bool res = JSValueOperations.valuesCompare(v1, v2) < 0.0f;
                    this.eval_stack.push(new JSValueBoolean(res));
                }
                break;

            case OpCodes.OpCode.IS_LTE:
                {
                    JSValue v2 = this.popValueEvalStack();
                    JSValue v1 = this.popValueEvalStack();
                    bool res = JSValueOperations.valuesCompare(v1, v2) <= 0.0f;
                    this.eval_stack.push(new JSValueBoolean(res));
                }
                break;

            case OpCodes.OpCode.IS_GT:
                {
                    JSValue v2 = this.popValueEvalStack();
                    JSValue v1 = this.popValueEvalStack();
                    bool res = JSValueOperations.valuesCompare(v1, v2) > 0.0f;
                    this.eval_stack.push(new JSValueBoolean(res));
                }
                break;

            case OpCodes.OpCode.IS_GTE:
                {
                    JSValue v2 = this.popValueEvalStack();
                    JSValue v1 = this.popValueEvalStack();
                    bool res = JSValueOperations.valuesCompare(v1, v2) >= 0.0f;
                    this.eval_stack.push(new JSValueBoolean(res));
                }
                break;

            case OpCodes.OpCode.INSTANCEOF:
                {
                    JSValue v2 = this.popValueEvalStack();
                    JSValue v1 = this.popValueEvalStack();

                    // TODO: Implement once we have prototype ...

                    this.eval_stack.push(new JSValueBoolean(true));
                }
                break;

            case OpCodes.OpCode.IN:
                {
                    JSValue v2 = this.popValueEvalStack();
                    JSValue v1 = this.popValueEvalStack();

                    // TODO: Implement later

                    this.eval_stack.push(new JSValueBoolean(true));
                }
                break;

            case OpCodes.OpCode.IS_EQ:
                {
                    JSValue v2 = this.popValueEvalStack();
                    JSValue v1 = this.popValueEvalStack();
                    bool res = JSValueOperations.valuesCompare(v1, v2).Equals(0.0f);
                    this.eval_stack.push(new JSValueBoolean(res));
                }
                break;

            case OpCodes.OpCode.IS_NEQ:
                {
                    JSValue v2 = this.popValueEvalStack();
                    JSValue v1 = this.popValueEvalStack();
                    bool res = !JSValueOperations.valuesCompare(v1, v2).Equals(0.0f);
                    this.eval_stack.push(new JSValueBoolean(res));
                }
                break;

            case OpCodes.OpCode.IS_SEQ:
                {
                    JSValue v2 = this.popValueEvalStack();
                    JSValue v1 = this.popValueEvalStack();
                    bool res = JSValueOperations.valuesStrictCompare(v1, v2).Equals(0.0f);
                    this.eval_stack.push(new JSValueBoolean(res));
                }
                break;

            case OpCodes.OpCode.IS_NSEQ:
                {
                    JSValue v2 = this.popValueEvalStack();
                    JSValue v1 = this.popValueEvalStack();
                    bool res = !JSValueOperations.valuesStrictCompare(v1, v2).Equals(0.0f);
                    this.eval_stack.push(new JSValueBoolean(res));
                }
                break;

            case OpCodes.OpCode.B_AND:
                {
                    JSValue v2 = this.popValueEvalStack();
                    JSValue v1 = this.popValueEvalStack();
                    this.eval_stack.push(JSValueOperations.valuesBitwiseAnd(v1, v2));
                }
                break;

            case OpCodes.OpCode.B_OR:
                {
                    JSValue v2 = this.popValueEvalStack();
                    JSValue v1 = this.popValueEvalStack();
                    this.eval_stack.push(JSValueOperations.valuesBitwiseOr(v1, v2));
                }
                break;

            case OpCodes.OpCode.B_XOR:
                {
                    JSValue v2 = this.popValueEvalStack();
                    JSValue v1 = this.popValueEvalStack();
                    this.eval_stack.push(JSValueOperations.valuesBitwiseXor(v1, v2));
                }
                break;

            case OpCodes.OpCode.L_AND:
                {
                    JSValue v2 = this.popValueEvalStack();
                    JSValue v1 = this.popValueEvalStack();
                    this.eval_stack.push(JSValueOperations.valuesLogicalAnd(v1, v2));
                }
                break;

            case OpCodes.OpCode.L_OR:
                {
                    JSValue v2 = this.popValueEvalStack();
                    JSValue v1 = this.popValueEvalStack();
                    this.eval_stack.push(JSValueOperations.valuesLogicalOr(v1, v2));
                }
                break;

            case OpCodes.OpCode.ASSIGN:
                {
                    JSValue v1 = this.popValueEvalStack();
                    JSValue v2 = this.eval_stack.pop();

                    if (v2.type != JSValue.Type.REFERENCE) {
                        this.throwRuntimeError("ReferenceError: Invalid left-hand size in assignment: " + v2.toString());
                        break;
                    }

                    this.eval_stack.push(v1);
                    this.referenceSet((JSValueReference)v2, v1);
                }
                break;

            case OpCodes.OpCode.RET:
                {
                    JSValue return_value = this.popValueEvalStack();
                    this.returnFromFunction(return_value);
                }
                break;

            case OpCodes.OpCode.JMP:
                {
                    int offset = this.readInteger(frame);
                    frame.ip += offset;
                }
                break;

            case OpCodes.OpCode.JF:
                {
                    int offset = this.readInteger(frame);
                    JSValueBoolean v1 = new JSValueBoolean(this.popValueEvalStack());

                    if (v1.getValue() == false) {
                        frame.ip += offset;
                    }
                }
                break;

            case OpCodes.OpCode.JT:
                {
                    int offset = this.readInteger(frame);
                    JSValueBoolean v1 = new JSValueBoolean(this.popValueEvalStack());

                    if (v1.getValue() == true) {
                        frame.ip += offset;
                    }
                }
                break;

            case OpCodes.OpCode.THROW:
                {
                    JSValue v1 = this.popValueEvalStack();
                    this.throwException(v1);
                }
                break;

            case OpCodes.OpCode.TRY_BEGIN:
                {
                    int handler_offset = this.readInteger(frame);

                    ExceptionHandlerContext exception_handler = new ExceptionHandlerContext();
                    exception_handler.absolute_ip = frame.ip + handler_offset;
                    exception_handler.caught_exception = null;

                    frame.exception_handlers.Push(exception_handler);
                }
                break;

            case OpCodes.OpCode.TRY_END:
                {
                    if (frame.exception_handlers.Count < 1) {
                        this.throwRuntimeError("Internal Error: TRY_END without exception handler context");
                    }
                    else {
                        frame.exception_handlers.Pop();
                    }
                }
                break;
        }
	}
}

}
