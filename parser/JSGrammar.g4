grammar JSGrammar;

options {
	language = 'CSharp_v4_0';
}

@lexer::members {
	public const int LT_CHANNEL = 2;
}

program:
	statements
;

statements:
	statement*
;

block:
	( statement | OPEN_CURLY_BRACKET  statements  CLOSE_CURLY_BRACKET )
;

statement:
	SEMICOLON |
	OPEN_CURLY_BRACKET statements CLOSE_CURLY_BRACKET |
	variablesDeclaration SEMICOLON |
	functionDeclarationHoisted |
	returnConstruct SEMICOLON |
	ifConstruct |
	CONTINUE_KEYWORD SEMICOLON |
	BREAK_KEYWORD SEMICOLON |
	whileConstruct |
	forConstruct |
	doWhileConstruct SEMICOLON |
	switchConstruct |
	tryCatchConstruct |
	throwConstruct SEMICOLON |
	expression SEMICOLON
;

returnConstruct:
	RETURN_KEYWORD  expression?
;

whileConstruct:
	WHILE_KEYWORD  '('  condition=expression  ')'  block?
;

forConstruct:
	FOR_KEYWORD  '('  ( variablesDeclaration | initialization=expression )?  ';' condition=expression? ';' postoperation=expression? ')' block?
;

doWhileConstruct:
	DO_KEYWORD OPEN_CURLY_BRACKET statements CLOSE_CURLY_BRACKET WHILE_KEYWORD '(' condition=expression ')'
;

switchCaseConstruct:
	( ( CASE_KEYWORD expression ) | DEFAULT_KEYWORD ) ':' statements
;

switchConstruct:
	SWITCH_KEYWORD '(' expression ')' OPEN_CURLY_BRACKET switchCaseConstruct* CLOSE_CURLY_BRACKET
;

tryCatchConstruct:
	TRY_KEYWORD OPEN_CURLY_BRACKET try_scope=statements CLOSE_CURLY_BRACKET CATCH_KEYWORD '(' IDENTIFIER? ')' OPEN_CURLY_BRACKET catch_scope=statements CLOSE_CURLY_BRACKET
;

throwConstruct:
	THROW_KEYWORD expression
;

ifConstruct:
	IF_KEYWORD '(' condition=expression ')' ( then=block (ELSE_KEYWORD otherwise=block)? )?
;

varDeclaration:
	IDENTIFIER ('=' expression)?
;

variablesDeclaration:
	VAR_KEYWORD varDeclaration (',' varDeclaration)*
;

expression:
	assignmentExpression |
	assignmentExpression ',' expression
;

assignmentExpression:
	conditionalExpression ((ASSIGN_OPERATOR | MULT_ASSIGN_OPERATOR | DIV_ASSIGN_OPERATOR | REM_ASSIGN_OPERATOR | ADD_ASSIGN_OPERATOR | SUB_ASSIGN_OPERATOR |
		LSHIFT_ASSIGN_OPERATOR | RSHIFT_ASSIGN_OPERATOR | ZFRSHIFT_ASSIGN_OPERATOR | AND_ASSIGN_OPERATOR | XOR_ASSIGN_OPERATOR | OR_ASSIGN_OPERATOR) assignmentExpression)?
;

conditionalExpression:
	logicalOrExpression (COND_OPERATOR then=assignmentExpression ':' otherwise=assignmentExpression)?
;

logicalOrExpression:
	logicalAndExpression (L_OR_OPERATOR logicalOrExpression)?
;

logicalAndExpression:
	bitwiseOrExpression (L_AND_OPERATOR logicalAndExpression)?
;

bitwiseOrExpression:
	bitwiseXorExpression (B_OR_OPERATOR bitwiseOrExpression)?
;

bitwiseXorExpression:
	bitwiseAndExpression (B_XOR_OPERATOR bitwiseXorExpression)?
;

bitwiseAndExpression:
	equalityExpression (B_AND_OPERATOR bitwiseAndExpression)?
;

equalityExpression:
	relationalExpression ((EQ_OPERATOR | NEQ_OPERATOR | SEQ_OPERATOR | NSEQ_OPERATOR) equalityExpression)?
;

relationalExpression:
	shiftExpression ((LT_OPERATOR | GT_OPERATOR | LTE_OPERATOR | GTE_OPERATOR | INSTANCEOF_KEYWORD | IN_KEYWORD) relationalExpression)?
;

shiftExpression:
	additiveExpression ((LSHIFT_OPERATOR | RSHIFT_OPERATOR | ZFRSHIFT_OPERATOR) shiftExpression)?
;

additiveExpression:
	multiplicativeExpression ((PLUS_OPERATOR | MINUS_OPERATOR) additiveExpression)?
;

multiplicativeExpression:
	unaryExpression ((MULT_OPERATOR | DIV_OPERATOR | REM_OPERATOR) multiplicativeExpression)?
;

unaryExpression:
	postfixExpression |
	(INCREMENT_OPERATOR | DECREMENT_OPERATOR | MINUS_OPERATOR | PLUS_OPERATOR | BITWISE_NOT_OPERATOR | LOGICAL_NOT_OPERATOR | DELETE_KEYWORD | TYPEOF_KEYWORD) unaryExpression
;

functionCallParameters:
	expression (',' expression)*
;

postfixExpression:
	callExpression (INCREMENT_OPERATOR | DECREMENT_OPERATOR)?
;

callExpression:
	expressionTerminal |
	NEW_KEYWORD callExpression (OPEN_PARANTHESIS functionCallParameters? CLOSE_PARANTHESIS)? |
	callExpression OPEN_PARANTHESIS functionCallParameters? CLOSE_PARANTHESIS |
	callExpression MEMBER_OPERATOR IDENTIFIER |
	callExpression OPEN_SQUARE_BRACKET expression CLOSE_SQUARE_BRACKET
;

expressionTerminal:
	OPEN_PARANTHESIS expression CLOSE_PARANTHESIS |
	THIS_KEYWORD |
	UNDEFINED_LITERAL |
	NULL_LITERAL |
	BOOLEAN_LITERAL |
	NUMERIC_LITERAL |
	STRING_LITERAL |
	REGEX_LITERAL |
	arrayLiteral |
	objectLiteral |
	functionDeclarationInExpression |
	IDENTIFIER
;

arrayLiteral:
	'[' (assignmentExpression (',' assignmentExpression)*)? ']'
;

objectLiteral:
	OPEN_CURLY_BRACKET (objectPropertyName ':' expression (',' objectPropertyName ':' expression)*)? CLOSE_CURLY_BRACKET
;
objectPropertyName:
	IDENTIFIER |
	STRING_LITERAL |
	NUMERIC_LITERAL
;

functionFormalParameters:
	IDENTIFIER (',' IDENTIFIER)*
;
functionDeclarationHoisted:
	FUNCTION_KEYWORD function_name=IDENTIFIER '(' functionFormalParameters? ')' OPEN_CURLY_BRACKET statements CLOSE_CURLY_BRACKET
;
functionDeclarationInExpression:
	FUNCTION_KEYWORD function_name=IDENTIFIER? '(' functionFormalParameters? ')' OPEN_CURLY_BRACKET statements CLOSE_CURLY_BRACKET
;



/* TOKENS */

fragment LineTerminators	: [\n\r\u2028\u2029] ;
LT							: [\n\r\u2028\u2029] -> channel(2) ;
/* LT							: LineTerminators ; */
WS							: ([ \t\f])+ -> skip ;
/* WS							: (LineTerminators | [ \t\f])+ -> skip ; */

COMMENT						: '/*' .*? '*/' (LT | WS)* -> skip;
LINE_COMMENT				: '//' ~( '\n' | '\r' | '\u2028' | '\u2029' )* (LT | WS)* -> skip;


SEMICOLON					: ';' ;
MULT_ASSIGN_OPERATOR		: '*=' ;
DIV_ASSIGN_OPERATOR			: '/=' ;
REM_ASSIGN_OPERATOR			: '%=' ;
ADD_ASSIGN_OPERATOR			: '+=' ;
SUB_ASSIGN_OPERATOR			: '-=' ;
LSHIFT_ASSIGN_OPERATOR		: '<<=' ;
RSHIFT_ASSIGN_OPERATOR		: '>>=' ;
ZFRSHIFT_ASSIGN_OPERATOR	: '>>>=' ;
AND_ASSIGN_OPERATOR			: '&=' ;
XOR_ASSIGN_OPERATOR			: '^=' ;
OR_ASSIGN_OPERATOR			: '|=' ;
ASSIGN_OPERATOR				: '=' ;

PLUS_OPERATOR				: '+' ;
MINUS_OPERATOR				: '-' ;
MULT_OPERATOR				: '*' ;
DIV_OPERATOR				: '/' ;
REM_OPERATOR				: '%' ;
MEMBER_OPERATOR				: '.' ;
OPEN_PARANTHESIS			: '(' ;
CLOSE_PARANTHESIS			: ')' ;
OPEN_SQUARE_BRACKET			: '[' ;
CLOSE_SQUARE_BRACKET		: ']' ;
OPEN_CURLY_BRACKET			: '{' ;
CLOSE_CURLY_BRACKET			: '}' ;
INCREMENT_OPERATOR			: '++' ;
DECREMENT_OPERATOR			: '--' ;
BITWISE_NOT_OPERATOR		: '~' ;
LOGICAL_NOT_OPERATOR		: '!' ;
LSHIFT_OPERATOR				: '<<' ;
RSHIFT_OPERATOR				: '>>' ;
ZFRSHIFT_OPERATOR			: '>>>' ;
LT_OPERATOR					: '<' ;
GT_OPERATOR					: '>' ;
LTE_OPERATOR				: '<=' ;
GTE_OPERATOR				: '>=' ;
EQ_OPERATOR					: '==' ;
NEQ_OPERATOR				: '!=' ;
SEQ_OPERATOR				: '===' ;
NSEQ_OPERATOR				: '!==' ;
B_AND_OPERATOR				: '&' ;
B_XOR_OPERATOR				: '^' ;
B_OR_OPERATOR				: '|' ;
L_AND_OPERATOR				: '&&' ;
L_OR_OPERATOR				: '||' ;
COND_OPERATOR				: '?' ;

VAR_KEYWORD					: 'var' ;
THIS_KEYWORD				: 'this' ;
FUNCTION_KEYWORD			: 'function' ;
RETURN_KEYWORD				: 'return' ;
IF_KEYWORD					: 'if' ;
ELSE_KEYWORD				: 'else' ;
BREAK_KEYWORD				: 'break' ;
CONTINUE_KEYWORD			: 'continue' ;
WHILE_KEYWORD				: 'while' ;
FOR_KEYWORD					: 'for' ;
DO_KEYWORD					: 'do' ;
SWITCH_KEYWORD				: 'switch' ;
CASE_KEYWORD				: 'case' ;
DEFAULT_KEYWORD				: 'default' ;
DELETE_KEYWORD				: 'delete' ;
TYPEOF_KEYWORD				: 'typeof' ;
INSTANCEOF_KEYWORD			: 'instanceof' ;
IN_KEYWORD					: 'in' ;
NEW_KEYWORD					: 'new' ;
TRY_KEYWORD					: 'try' ;
CATCH_KEYWORD				: 'catch' ;
FINALLY_KEYWORD				: 'finally' ;
THROW_KEYWORD				: 'throw' ;

UNDEFINED_LITERAL			: 'undefined' ;
NULL_LITERAL				: 'null' ;
BOOLEAN_LITERAL				: 'true' | 'false' ;
NUMERIC_LITERAL				: 'NaN' | 'Infinity' | DecimalLiteral | HexadecimalLiteral ;
STRING_LITERAL				: '"' DoubleStringCharacter* '"' | '\'' SingleStringCharacter* '\'' ;
REGEX_LITERAL				: 'r"' .*? '"' ;

IDENTIFIER					: [_$A-Za-z][_$A-Za-z0-9]* ;

fragment DecimalLiteral :
	'0' |
	[1-9] [0-9]* '.'? [0-9]* DecimalExponent? |
	'.' [0-9]+ DecimalExponent?
;
fragment DecimalExponent :
	('e' | 'E') ('+' | '-')? [0-9]+
;
fragment HexadecimalLiteral:
	'0' ('x' | 'X') [0-9A-Fa-f]+
;

fragment SingleStringCharacter:
	~('\'' | '\\' | [\n\r\u2028\u2029]) |
	'\\' StringEscapeSequence
;
fragment DoubleStringCharacter:
	~('"' | '\\' | [\n\r\u2028\u2029]) |
	'\\' StringEscapeSequence
;
fragment StringEscapeSequence:
	'\'' | '"' | '\\' | 'b' | 'f' | 'n' | 'r' | 't' | 'v' |
	LineTerminators |
	'0' |
	'x' [0-9A-Fa-f] [0-9A-Fa-f]
;

