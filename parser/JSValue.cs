/*
 *  JSValue.cs
 *
 *  Author:
 *       Bogdan Blagaila <blbogdan2000@gmail.com>
 *
 *  Copyright (c) 2014 Bogdan Blagaila
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

using System;

namespace kjs.parser {

public class JSValue {
	public enum Type {
		NONE = 0,
		UNDEFINED,
		NULL,
		BOOLEAN,
		NUMBER,
		STRING,
		REGEX,
		ARRAY,
		OBJECT,
		FUNCTION,
		REFERENCE
	};
	
	public Type type;

	public JSValue(Type type) {
		this.type = type;
	}

	public string getTypeString() {
		switch (this.type) {
			case Type.UNDEFINED:
				return "undefined";

			case Type.NULL:
				return "null";

			case Type.BOOLEAN:
				return "boolean";

			case Type.NUMBER:
				return "number";

			case Type.STRING:
				return "string";

			case Type.OBJECT:
				return "object";

			case Type.ARRAY:
				return "array";

			case Type.FUNCTION:
				return "function";

			case Type.REFERENCE:
				return "[[reference]]";

			default:
				break;
		}

		return "#invalid";
	}

	public virtual bool isObject() {
		return false;
	}

	public virtual string toString() {
		switch (type) {
			case Type.UNDEFINED:
				return "undefined";

			case Type.NULL:
				return "null";

			default:
				break;
		}

		return "@value";
	}

	public static JSValue Undefined() {
		return new JSValue(Type.UNDEFINED);
	}

	public static JSValue Null() {
		return new JSValue(Type.NULL);
	}
}

}
