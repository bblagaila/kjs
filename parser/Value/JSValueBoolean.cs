﻿//
//  JSValueBoolean.cs
//
//  Author:
//       Bogdan <${AuthorEmail}>
//
//  Copyright (c) 2015 Bogdan
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//

using System;

namespace kjs.parser.Value {

public class JSValueBoolean : JSValue {
	public bool booleanValue;

	public JSValueBoolean(bool value) : base(JSValue.Type.BOOLEAN) {
		this.booleanValue = value;
	}

	public JSValueBoolean(JSValue value) : base(JSValue.Type.BOOLEAN) {
		switch (value.type) {
			case JSValue.Type.NULL:
			case JSValue.Type.UNDEFINED:
				this.booleanValue = false;
				break;

			case JSValue.Type.BOOLEAN:
				this.booleanValue = ((JSValueBoolean)value).booleanValue;
				break;

			case JSValue.Type.NUMBER:
				if (((JSValueNumber)value).numberValue.Equals(0.0f)) {
					this.booleanValue = false;
				}
				else {
					this.booleanValue = true;
				}
				break;

			case JSValue.Type.STRING:
				if (((JSValueString)value).stringValue.Length == 0) {
					this.booleanValue = false;
				}
				else {
					this.booleanValue = true;
				}
				break;

			case JSValue.Type.ARRAY:
			case JSValue.Type.FUNCTION:
			case JSValue.Type.OBJECT:
			case JSValue.Type.REGEX:
				this.booleanValue = true;
				break;
		}
	}

	public static JSValueBoolean True() {
		return new JSValueBoolean(true);
	}

	public static JSValueBoolean False() {
		return new JSValueBoolean(false);
	}

	public bool getValue() {
		return this.booleanValue;
	}

	public override string toString() {
		return this.booleanValue ? "true" : "false";
	}
}

}
