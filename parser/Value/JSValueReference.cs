//
//  JSValueReference.cs
//
//  Author:
//       Bogdan Blagaila <blbogdan2000@gmail.com>
//
//  Copyright (c) 2014 Bogdan Blagaila
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//

using System;

namespace kjs.parser.Value {

public class JSValueReference : JSValue {
	public JSValueObject refObject;
	public string refProperty;
	
	public JSValueReference(JSValueObject obj, string property) : base(JSValue.Type.REFERENCE) {
		this.refObject = obj;
		this.refProperty = property;
	}
	
	public override string toString() {
		return "[[ref " + (this.refObject == null ? "<null> " : ("<" + this.refObject.GetHashCode() + ">.")) + this.refProperty + "]]";
	}
}

}
