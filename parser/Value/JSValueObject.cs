/*
 *  JSValueObject.cs
 *
 *  Author:
 *       Bogdan Blagaila <blbogdan2000@gmail.com>
 *
 *  Copyright (c) 2014 Bogdan Blagaila
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

using System;
using System.Collections;
using System.Collections.Generic;

namespace kjs.parser.Value {

public class JSValueObject : JSValue {
	public abstract class Property {
		public enum Type {
			NONE = 0,
			DATA,
			ACCESSOR
		};

		public bool enumerable;
		public bool configurable;

		public abstract Type getType();
	}

	public class DataProperty : Property {
		public JSValue value;
		public bool writable;

		public DataProperty(JSValue value, bool writable = true, bool enumerable = true, bool configurable = true) {
			this.enumerable = enumerable;
			this.configurable = configurable;

			this.value = value;
			this.writable = writable;
		}

		public override Type getType() {
			return Type.DATA;
		}
	}

	public class AccessorProperty : Property {
		public JSValueFunction getter;
		public JSValueFunction setter;

		public override Type getType() {
			return Type.ACCESSOR;
		}
	}

	/*
	 * Object properties
	 */
	public Dictionary<string, Property> properties = new Dictionary<string, Property>();

	public string name = "Object";

	public JSValue prototype = null;

	public bool extensible = true;

	/**
	 * Constructor
	 */
	public JSValueObject() : base(JSValue.Type.OBJECT) {
	}

	public JSValueObject(JSValue.Type type) : base(type) {
	}

	public override bool isObject() {
		return true;
	}

	/*
	 * ECMA Methods
	 */
	public JSValue getPrototypeOf() {
		return this.prototype;
	}

	public bool setPrototypeOf(JSValue prototype) {
		if (this.isExtensible()) {
			this.prototype = prototype;
			return true;
		}
		if (this.prototype == prototype) {
			return true;
		}
		return false;
	}

	public bool isExtensible() {
		return this.extensible;
	}

	public bool preventExtensions() {
		/* TODO: Right behavior ? */
		this.extensible = false;
		return true;
	}

	public Property getOwnProperty(string key) {
		return this.properties[key];
	}

	public bool hasProperty(string key) {
		return this.get(key) != null;
	}

	public Property get(string key) {
		Property ret = null;
		JSValueObject obj = this;

		while (obj != null) {
            if (obj.properties.TryGetValue(key, out ret)) {
                break;
            }

            if (obj.prototype != null && obj.prototype.isObject()) {
                obj = (JSValueObject)obj.prototype;
            }
            else {
                obj = null;
            }
		}

		return ret;
	}

	public Property getByIndex(int index) {
		String key = index.ToString();
		return this.get(key);
	}

	public bool set(string key, JSValue value) {
		Property property;

		property = this.get(key);
		if (property != null) {
			if (property.getType() == Property.Type.ACCESSOR) {
				/* TODO: Inform caller a JS evaluation is needed */
				return false;
			}

			if (((DataProperty)property).writable != true) {
				/* TODO: Refactor after above refactoring */
				// return false;
				return true;
			}

			((DataProperty)property).value = value;
			return true;
		}

		property = new DataProperty(value);
		this.properties[key] = property;

		return true;
	}

	public bool setByIndex(int index, JSValue value) {
		string key = index.ToString();
		return this.set(key, value);
	}

	public bool delete(string key) {
		Property property = this.get(key);

		if (property != null) {
			if (property.getType() == Property.Type.DATA && ((DataProperty)property).writable != true) {
				return false;
			}

            /* Chrome removes accessors */
            /*
			if (property.getType() == Property.Type.ACCESSOR) {
				return false;
			}
            */
		}

		this.properties.Remove(key);
		return true;
	}

	public virtual bool defineOwnProperty(String key, Property property) {
		Property old_property;

		old_property = this.properties[key];
		if (old_property != null) {
			if (old_property.getType() == Property.Type.ACCESSOR) {
				return false;
			}

			if (old_property.getType() == Property.Type.DATA && ((DataProperty)old_property).writable != true) {
				return false;
			}
		}

		this.properties[key] = property;
		return true;
	}

	public string[] enumerate() {
		List<string> result = new List<string>();
		JSValueObject obj = this;

		while (obj != null) {
			foreach (KeyValuePair<string, Property> entry in this.properties) {
				if (entry.Value.enumerable != true) {
					continue;
				}

				result.Add(entry.Key);
			}

            if (obj.prototype.isObject()) {
                obj = (JSValueObject)obj.prototype;
            }
            else {
                obj = null;
            }
		}

		return result.ToArray();
	}

	public string[] ownPropertyKeys() {
		List<string> result = new List<string>();

		foreach (KeyValuePair<string, Property> entry in this.properties) {
			if (entry.Value.enumerable != true) {
				continue;
			}

			result.Add(entry.Key);
		}

		return result.ToArray();
	}

    /* Non-ECMA Methods */

    public JSValue getDataValue(string key) {
        Property property = this.get(key);

        if (property != null && property.getType() == Property.Type.DATA) {
            return ((DataProperty)property).value;
        }

        return JSValue.Undefined();
    }

	public override string toString() {
		return "[Object " + this.name + "]";
	}

}

}
