/**
 *  JSValueFunction.cs
 *
 *  Author:
 *       Bogdan Blagaila <blbogdan2000@gmail.com>
 *
 *  Copyright (c) 2014 Bogdan Blagaila
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

using System;

namespace kjs.parser.Value {

public class JSValueFunction : JSValueObject {
	/* Function object methods */
	public JSValue call(JSValue _this, JSValue[] arguments) {
		/* TODO: Implement ... */
		return null;
	}

	public JSValue construct(JSValue[] arguments, JSValue obj) {
		/* TODO: Implement ... */
		return null;
	}
	
	/* TODO: Refactor below ... */

	public delegate JSValue NativeFunction(JSValue _this, JSValue[] arguments);

	public string name;
	public string[] formal_parameters = null;
	public string[] local_variables = null;
	public JSValueObject[] closure = null;
	public JSValueObject bound_this = null;
	public NativeFunction native_function = null;
	public byte[] code_ref = null;
	public int code_start_offset = -1;

	public JSValueFunction() : base(JSValue.Type.FUNCTION) {
		this.set("prototype", new JSValueObject());
	}

    public override bool defineOwnProperty(string key, Property property) {
        if (key == "prototype") {
            return true;
        }

        return base.defineOwnProperty(key, property);
    }

    public static JSValueFunction createNative(string name, NativeFunction function) {
		JSValueFunction ret = new JSValueFunction();

		ret.name = name;
		ret.formal_parameters = new string[0];
		ret.local_variables = new string[0];
		ret.closure = new JSValueObject[0];
		ret.native_function = function;

		return ret;
	}

	public bool isNative() {
		return native_function != null;
	}

	public override string toString() {
		if (this.isNative()) {
			return "function " + this.name + "() { [native code] }";
		}
		else {
			return "function " + this.name + "() { " + "code@" + this.code_start_offset + " }";
		}
	}
}

}
