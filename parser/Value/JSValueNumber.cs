//
//  JSValueNumber.cs
//
//  Author:
//       Bogdan Blagaila <blbogdan2000@gmail.com>
//
//  Copyright (c) 2014 Bogdan Blagaila
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//

using System;

namespace kjs.parser.Value {

public class JSValueNumber : JSValue {
	public float numberValue;
	
	public JSValueNumber(float value) : base(JSValue.Type.NUMBER) {
		this.numberValue = value;
	}

	public JSValueNumber(string value) : base(JSValue.Type.NUMBER) {
		if (float.TryParse(value, out this.numberValue) == false) {
			this.numberValue = float.NaN;
		}
	}

	public JSValueNumber(JSValue value) : base(JSValue.Type.NUMBER) {
		if (value.type == JSValue.Type.NUMBER) {
			this.numberValue = ((JSValueNumber)value).numberValue;
		}
		else if (value.type == JSValue.Type.NULL) {
			this.numberValue = 0;
		}
		else if (value.type == JSValue.Type.BOOLEAN) {
			if (((JSValueBoolean)value).booleanValue == true) {
				this.numberValue = 1;
			}
			else {
				this.numberValue = 0;
			}
		}
		else if (float.TryParse(value.toString(), out this.numberValue) == false) {
			this.numberValue = float.NaN;
		}
	}
	
	public float getValue() {
		return this.numberValue;
	}

	public bool isNaN() {
		return float.IsNaN(this.numberValue);
	}
	
	public override string toString() {
		if (float.IsNaN(numberValue)) {
			return "NaN";
		}
		else if (float.IsInfinity(numberValue) || float.IsPositiveInfinity(numberValue)) {
			return "#inf";
		}
		else if (float.IsNegativeInfinity(numberValue)) {
			return "-#inf";
		}
		return "" + this.numberValue;
	}
}

}
