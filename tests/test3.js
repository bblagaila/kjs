var c = {
	test: function() {
		return { a: 99 };
	}
};

d = c.test().a--;
e = c.test()['a']++;
f = c.test().a++;

a = 99;
g = a++;
h = a--;
i = --a;
