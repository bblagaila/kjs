﻿
$=3;
a=$+1;
$_ = a;
$_ = $_ + 1;
$ = {};
$.a = a;
$.b = $_;
$.c = {o: 3};
$.c.a = $.b + 200;
$.c.b = $.a + 100;
x = "hello, world." + "10" + 5;
x = x + 5;

a = 1, 5;

this.b = 99;
this.b++;

o = 0 / 0;

p = 3 << 1;

lt = p < 9;
nlt = p < 3;

v = 6 & 2;

k = p < 9 ? 1 : 2 + 3;
l = p >= 9 ? 1 : 3 + 5;

t = {};
if (p < 9) {
	t.a = 1;
}
else {
	t.b = 3;
}

t.z = .1 + .2 - .3;

x1 = 1;
x2 = 1;
i = 0;

while (i < 10) {
	x = x1 + x2;
	x1 = x2;
	x2 = x;
	i++;
}

test = 0;

test2 = function runFibo() {
	var num_fibo = 15;

	function fibo(n) {
		if (n == 0) {
			return 1;
		}
		else if (n == 1) {
			return 1;
		}

		return fibo(n-1) + fibo(n-2);
	};

	return fibo(num_fibo + 1);
};

var iterfibo = function(n) {
	var x = 1, x1 = 1, x2 = 1, i = 0;

	while (i < n - 1) {
		x = x1 + x2;
		x1 = x2;
		x2 = x;
		i++;
	}

	return x;
};

test3 = test2();
test4 = iterfibo(16);

var add_evens = function(n) {
	for (var s = 0, i = 0; i < n; i++) {
		if (i % 2 != 0) {
			continue;
		}

		if (i > 5) {
			break;
		}

		s = s + i;
	}

	return s;
};

test5 = add_evens(11);

console.log("The result is: " + test5);

var fibo_until = 100;

var testfn = function ttt() {
	var n = 120;

	function getFibo() {
		for (var x, x1 = 1, x2 = 1, i = 1; i < n; i++) {
			x = x1 + x2;
			x1 = x2;
			x2 = x;
		}
		return x2;
	}

	return getFibo(fibo_until);
};

console.log(testfn());

console.log("Prepare..." + 1);

console.dump = function() {
	console.log("Dumping our object:\n " + this);
};
console.dump();

test15 = 0;
test16 = { test17: 3 };
delete test15;
delete test16.test17;
delete 3;

var constructor = function TestObject() {
	this.name = "A test";
};

constructor.prototype.log = function(msg) {
	console.log("-- Log: " + msg);
};

var obj = new constructor;
obj.log(obj);

var construct2 = function (name) {
	this.name = name;
};
construct2.prototype.log = function(msg) {
	console.log("-- Logging from [" + this.name + "]: " + msg);
};

/* this Would be a sintactial error ; right ? ; no! cause i fixed comments */

var test_variable = (a);

var obj2 = new construct2("Tester");
obj2.log(obj2);
