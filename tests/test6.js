﻿undefined;
null;
true;
false;
4;
4.5;
"abc";
{};
a;
{a: 1, "b": 2, 'c': 3, d: a};
d['b'].c.f(a, b, 2);
i++;
new constructor(1, a, b+c);
++i;
delete a;
typeof {};
typeof f.a;
-a;
+b;
1 * 4;
4 + 5 * 3;
5 - 4;
2 << 2;
a + b * (4 << c);
1 < 5;
5 <= 1;
a > 5;
7 >= c;
4 in c;
b in c;
b instanceof c;
3 == 4;
3 != 5;
a === b;
c !== d;
a & 1;
b | 3;
7 ^ a;
a && b;
c || d;
a && 1 ? a : b;
a ? b : c;
x = y;
y = a + 5;
z *= a;
null;
var a, b = 5, c = x;
if (a) {
	a = a + 1;
}
else {
	a = a + 2;
}
null;
for (;;);
for (i = 0;;);
for (var iter = 5;;);
null;
for (i = 0; i < 10; i++);
for (i = 0; i < 15; ++i) {
    log(i);
}
null;
for ( ; i < 100; ++i);
for ( ; ; i++);
for ( ; i < 50; );
null;
for (i = 0; i < 100; i++) {
    if (i > 5) { break; }
    if (x == 1) { continue; }
    a = 1;
}
null;
for (i = 0; i < 5; i++) {
    for (j = 0; j < 6; j++) {
        continue;
        a = 1;
    }
    break;
}
null;
while (i < 30) {
    i++;
    if (a == 1) {
        continue;
    }
    if ( i > 15) {
        break;
    }
}
null;
while (a);
undefined;
function a(b, c, d, e, f) {
    function b() {
        return 5;
    }

    (function c() {
        return 2;
    })();

    var g = function c() {
        return b();
    };

    return g;
}
null;
do {
    x = x + 1;
} while (x < 10);
undefined;
do {
    x ^= b;
    if (x == 0) {
        break;
    }
    else {
        continue;
    }

    x = x + 1;
} while (x < 10);
null;
switch (x) {
    case 1:
        a = 1;
    case 2:
        b = 2;
        break;

    case y:
        z = 1;
        break;

    default:
        z = 0;
        break;
}
undefined;
switch (w) {
    default:
        s = 'none';
    case y.a:
        s = "a";
        break;
    case y.c * 5:
        s = "c1";
    case y.b:
        s = "b";
    case y.c * 5:
        s = "c2";
        break;
    case y:
        s = "y";
        break;
        /*
    default:
        s="none";
        break;
        */
}
undefined;
try {
    if (X == 0) {
        throw y;
    }
    else if (x == 1) {
        console.log(x);
    }

    x = x + 1;
}
catch (e) {
    console.log(e);
}
null;
try {
    throw 42;
}
catch () {
}
undefined;
