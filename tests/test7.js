﻿console.log("Hello, world!");

c = {};

c.a = 1;
c.b = 2 + 3;
c.c = 4 + 2 * 5;
c.d = 1 << 4;
c.e = (true ? 1 : 5);

c.f = function fibo(n) {
    x1 = 1;
    x2 = 1;

    for (var i = 0; i < n; i++) {
        x = x1 + x2;
        x1 = x2;
        x2 = x;

        if (i % 10 == 0 || i == n - 1) {
            console.log("Fibo(" + i + ") = " + x);
        }
    }

    return true;
};

/*
if (c.f(100)) {
    console.log("Fibo Done.");
}
*/

function fibo(n) {
    var ret = 1, x1 = 1, x2 = 1, x;

    for (var i = 0; i < n; i++) {
        x = x1 + x2;
        x1 = x2;
        x2 = x;
    }

    return x;
}

// console.log(fibo(100));

function prime(n) {
    var k = 0;
    var iter = 0;

    for (var i = 2; i <= 2000000000; i++) {
        var is_prime = true;

        for (var j = 2; j < i / 2; j++) {
            iter++;
            if (i % j == 0) {
                is_prime = false;
                break;
            }
        }

        if (is_prime) {
            k++;
            if (k >= n) {
                console.log("" + iter + " iterations.");
                return i;
            }
        }
    }
}

console.log(prime(10));

try {
    if (a == 4) {
        a = 5;
    }
    throw a;
}
catch (e) {
    console.log("Excep: " + e);
}

try {
    e = 2;

    try {
        console.log(e);
        console.log(b);
    }
    catch (e) {
        console.log(e);
        console.log(a);
    }

    throw e;
}
catch (y) {
    console.log(y);
    a = a + 1;
}
